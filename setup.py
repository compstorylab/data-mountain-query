from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt") as f:
    libs = f.read().splitlines()

setup(
    name="data_mountain_query",
    version="0.1.0",
    description="Python API for Computational Story Lab Tweet Database",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    keywords="NLP twitter ngrams",
    url="https://gitlab.com/compstorylab/data-mountain-query",
    author="Michael Arnold",
    author_email="michael.arnold@uvm.edu",
    packages=find_packages(),
    package_data={'tweet_query': ['pkg_data/*.bin',
                                  'pkg_data/*.py',
                                  'pkg_data/*.json',
                                  'data/ambient/*.json.gz',
                                  'data/*']},
    python_requires=">=3.7",
    install_requires=libs,
    license="MIT",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Topic :: Text Processing :: Linguistic",
        "Operating System :: OS Independent",
    ],
)