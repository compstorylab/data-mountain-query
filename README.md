# DataMountain Query

***


Tools for working with Twitter data stored in MongoDB,
including database connections, parsing, visualization and sentiment analysis.

Core class `Counters()` handles:


* Database Connections
* Local Caching
* Query Metadata
* Aggregation
* Timeseries Plotting
* Sentiment Timeseries
* Sentiment Shifts 

# Installation

If you don't already have an anaconda distribution, here's a 
link to install miniconda
https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html.
Miniconda is a lightweight python package manager.


Ideally, you'll create a new conda environment using something like:

```conda create -n data-mountain-query --channel default --channel conda-forge python=3.9```

and activate it using:

```conda activate data-mountain-query```

Finally, install a few essential packages using:

```conda install -c conda-forge pip numpy pandas matplotlib scipy scikit-learn```

#### Install
You can install this package statically by cloning the repo and running

    git clone https://gitlab.com/compstorylab/data-mountain-query.git
    cd data-mountain-query
    python setup.py install

#### Install Development Version
You can install this package for development by cloning the repo and running
the included `setup.py` script in your terminal.

    git clone https://gitlab.com/compstorylab/data-mountain-query.git
    cd data-mountain-query
    pip install -e .

This will allow the module to load directly from the cloned git repo, so changes will be reflected anywhere the module is imported.

#### Install ipykernel for Juypter notebooks.

To use jupyter notebooks you'll also need to install the ipykernel for this environment. Make sure you are within your choosen virtual environment when running this command:

```python -m ipykernel install --user --name data-mountain-query --display-name "Python (data-mountain-query)"```

Now, you should be able to start a notebook on https://vacc-ondemand.uvm.edu and select the newly available kernel Python (data-mountain-query).

#### Install credentials file

To access the database, reach out to Michael Arnold mvarnold@uvm.edu for credentials.

The package looks in `~/.data_mountain_query/.env` for these credientials, which should look like:

```
USER_NAME = 'myuser'
PASSWORD = 'mypassword'
```

## Usage

The first step to using this package is to test the database connection.

```python
from data_mountain_query.connection import get_connection
from pprint import pprint
collection, client = get_connection()
pprint(collection.find_one())
```

Here `collection` is a standard pymongo collection object. You could create your own queries and aggregations from this, but
we've collected some standardized usages in `query.py` and `aggregation.py`.


An example of accessing raw tweets:
```python 
from data_mountain_query.query import get_ambient_tweets
from data_mountain_query.connection import get_connection
import pandas as pd
from datetime import datetime
from pprint import pprint

anchor =  "Capitol"
start_date = datetime(2021,1,1)
end_date = datetime(2021,2,1)
dates = pd.date_range(start_date, end_date, freq='6H')

collection, client = get_connection()
tweets = [t for t in get_ambient_tweets(anchor, dates, collection)]
pprint(tweets[0])
```

The core of the package is the `Counters` object, which hosts a range of functionality in its methods.

Querying for tweets matching a keyword, and ngram parsing can be done concisely:

```python
from datetime import datetime
import pandas as pd
from data_mountain_query.counters import AmbientTweetCounters

anchor =  "Capitol"
start_date = datetime(2021,1,1)
end_date = datetime(2021,2,1)
dates = pd.date_range(start_date, end_date, freq='6H')

counters = AmbientTweetCounters(dates, anchor) # object initialization

counters.query(p=.01) # query
print(counters.counters[0])
```

Counters objects also contain plotting methods for fast, standardized visualizations. 
Given an existing `counters` object, we can easily plot sentiment time series, and 

```python
counters.plot_sentiment_timeseries()

```

<!-- 
## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->
