import seaborn as sns
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
from datetime import datetime
import shifterator as sh
import matplotlib.dates as mdates
from data_mountain_query.sentiment import grab_hedonometer, get_score_distribution, load_happs_scores, filter_by_scores, get_weighted_score_np

from mpl_toolkits.axes_grid1 import make_axes_locatable

color_dict = {'RT':'orange',
              'OT':'blue'}


def error_bars(timeseries, window=7):
    """ Given a timeseries return confidence interval arrays"""

    std = timeseries.rolling(window,center=True, min_periods=1).std()*2
    upper = timeseries+std
    lower = timeseries-std
    print(f'number nans: {sum(upper.isna())}')
    mean = timeseries.mean()
    return lower.fillna(mean-std.mean()*2), upper.fillna(mean+std.mean()*2)


def weighted_average(df, count_df, word, n=7):
    """ Computes weighted average for sentiment using number of ngrams as a weight
    :param df: Pandas DataFrame of ngram Sentiment
    :param count_df: Pandqas DataFrame of ngram counts used to score sentiment
    :param word: string index for dataframe
    :param n: window size
    """
    index = df[word].isna()
    count_df[word][index] = np.nan
    weighted = count_df[word] * df[word]
    rolling_weights = count_df[word].rolling(n,
                                             center=True,
                                             min_periods=3,
                                             win_type='triang',
                                             ).mean()
    weighted_mean = weighted.rolling(n,
                                     center=True,
                                     min_periods=3,
                                     win_type='triang',
                                     ).mean() / rolling_weights
    return weighted_mean



def sentiment_timeseries_plot(df,
                             count_df,
                             std_df,
                             word,
                             ngrams='1grams',
                             fig=None,
                             ylims=(4.5,7.0),
                             reference=True,
                             pth="",
                             count_log=True,
                             lang='en',
                             anchor_attention=False,
                             count_type='count',
                             hedonometer=True,
                             label='',
                             color=None,
                             ):
    """ Plot sentiment timeseries

    # ToDo: Add storywrangler counts in the top plot
    # ToDo: replace # n-grams optionally with anchor volume from storywrangler
    # ToDo: plot count, and count_no_RT
    """
    if color is None:
        color_dict = {'count': '#1f77b4',
                      'count_no_rt': '#b41f33',
                    }
    else:
        color_dict = {
                      'count_no_rt': color,
                      }

    label_dict = {'count': '[All]',
                  'count_no_rt': f'[noRT] {label}',
                }
    print(f'Plotting: {word} {label_dict[count_type]}')
    part = True # is this function being called as a part of a grid plot?
    if fig is None:
        f, ax = plt.subplots(nrows=3, ncols=1, sharex=True,
                             figsize=(5,5), dpi=500,
                             gridspec_kw={'height_ratios':[1,3,1]},
                             )
        part = False
    else:
        f,ax = fig


    # plot n-gram counts
    plt.setp(ax[0].get_xticklabels(), visible=False)

    if not anchor_attention:
        if count_log:
            ax[0].semilogy(count_df[word].index,
                           count_df[word],
                           '.', ms=5, alpha=0.2, nonpositive='mask', color=color_dict[count_type]
                           )
            ax[0].semilogy(count_df[word].index,
                           count_df[word].rolling(5, center=True, min_periods=3).mean(),
                           lw=2, nonpositive='clip', color=color_dict[count_type]
                           )

        else:
            ax[0].plot(count_df[word].index,
                       count_df[word],
                       '.', ms=5, alpha=0.2, color=color_dict[count_type]
                       )
            ax[0].plot(count_df[word].index,
                       count_df[word].rolling(5, center=True, min_periods=3).mean(),
                       lw=1, color=color_dict[count_type]
                       )
    else:
        # ToDo: add this
        raise NotImplementedError("Grab storywrangler data, and plot with rolling average")


    # n-gram count fine tuning
    ax[0].set_ylabel('\n # n-grams \n',fontsize=14)

    try:
        ax[1].fill_between(std_df[word].index,
                    df[word]-std_df[word]/np.sqrt(count_df[word]/10),
                    df[word]+std_df[word]/np.sqrt(count_df[word]/10),
                    step='post',
                    alpha=0.4,
                    color=color_dict[count_type],
                    zorder=1,)
    except:
        pass
    ax[2].plot(std_df[word].index,
               std_df[word],
               '.', ms=5,
               alpha=0.2,
               color=color_dict[count_type],
               zorder=1, )

    ax[2].plot(std_df[word].index,
               std_df[word].rolling(7, center=True, min_periods=3).mean(),
               alpha=0.8,
               lw=2,
               color=color_dict[count_type],
               zorder=1, )

    # plot hedonometer data
    start_date = df.index[0]
    end_date = df.index[-1]
    if hedonometer:
        try:
            df_ref = grab_hedonometer(start_date, end_date, lang=lang)
            happs_index = []
            happs_data = []
            for i,x in df_ref.items():
                happs_index.append(i)
                happs_index.append(i)
                happs_index.append(i)
                happs_data.append(x)
                happs_data.append(np.nan)
                happs_data.append(x)

            ax[1].plot(happs_index, happs_data, '-',
                    color='#9e9e9e',
                    zorder=0,
                    label=f"All Twitter [{lang}]",
                    )
        except KeyError:
            print(f"Language - {lang} is not available")
            pass

    ax[1].plot(df[word].index, df[word],
            zorder=2,
            color=color_dict[count_type],
            label=f'{word} {label_dict[count_type]}',
            drawstyle='steps-post')

    ax[1].xaxis.set_tick_params(rotation=45, labelsize=16)
    ax[1].set_ylim([ylims[0],ylims[1]])
    ax[1].set_ylabel("Ambient Sentiment", fontsize=16)

    if not part:
        locator = mdates.MonthLocator([i * 2 + 1 for i in range(6)])
        formatter = mdates.DateFormatter('%b %Y')
        ax[1].xaxis.set_major_locator(locator)
        ax[1].xaxis.set_major_formatter(formatter)
        ax[0].set_title(word, fontsize=20)
        ax[1].set_ylabel("Ambient Sentiment", fontsize=16)
        ax[1].set_xlabel("Date", fontsize=16)
        ax[1].grid(which='major', axis='x', zorder=-2)
        ax[1].set_axisbelow(True)
        ax[1].legend(fancybox=False)
        plt.tight_layout()
        plt.savefig(f'{pth}sentiment_timeseries_{word}{ngrams}.png', dpi=500)
        plt.savefig(f'{pth}sentiment_timeseries_{word}{ngrams}.pdf')

    return ax

def pds_timeseries_plot(df,
                         count_df,
                         std_df,
                         word,
                         score_dict='danger',
                         ngrams='1grams',
                         fig=None,
                         ylims=(4.5,7.0),
                         reference=True,
                         pth="",
                         count_log=True,
                         lang='en',
                         anchor_attention=False,
                         count_type='count',
                         hedonometer=True,
                         label='',
                         color=None,
                         ):
    """ Plot PDS timeseries

    # todo:
        * change hedonometer api call
        * add pds series


    """
    if color is None:
        color_dict = {'count': '#1f77b4',
                      'count_no_rt': '#b41f33',
                    }
    else:
        color_dict = {
                      'count_no_rt': color,
                      }

    label_dict = {'count': '[All]',
                  'count_no_rt': f'[noRT] {label}',
                }
    print(f'Plotting: {word} {label_dict[count_type]}')
    part = True # is this function being called as a part of a grid plot?
    if fig is None:
        f, ax = plt.subplots(nrows=3, ncols=1, sharex=True,
                             figsize=(5,5), dpi=500,
                             gridspec_kw={'height_ratios':[1,3,1]},
                             )
        part = False
    else:
        f,ax = fig


    # plot n-gram counts
    plt.setp(ax[0].get_xticklabels(), visible=False)

    if not anchor_attention:
        if count_log:
            ax[0].semilogy(count_df[word].index,
                           count_df[word],
                           '.', ms=5, alpha=0.2, nonpositive='mask', color=color_dict[count_type]
                           )
            ax[0].semilogy(count_df[word].index,
                           count_df[word].rolling(5, center=True, min_periods=3).mean(),
                           lw=2, nonpositive='clip', color=color_dict[count_type]
                           )

        else:
            ax[0].plot(count_df[word].index,
                       count_df[word],
                       '.', ms=5, alpha=0.2, color=color_dict[count_type]
                       )
            ax[0].plot(count_df[word].index,
                       count_df[word].rolling(5, center=True, min_periods=3).mean(),
                       lw=1, color=color_dict[count_type]
                       )
    else:
        # ToDo: add this
        raise NotImplementedError("Grab storywrangler data, and plot with rolling average")


    # n-gram count fine tuning
    ax[0].set_ylabel('\n # n-grams \n',fontsize=14)

    try:
        ax[1].fill_between(std_df[word].index,
                    df[word]-std_df[word]/np.sqrt(count_df[word]/10),
                    df[word]+std_df[word]/np.sqrt(count_df[word]/10),
                    step='post',
                    alpha=0.4,
                    color=color_dict[count_type],
                    zorder=1,)
    except:
        pass
    ax[2].plot(std_df[word].index,
               std_df[word],
               '.', ms=5,
               alpha=0.2,
               color=color_dict[count_type],
               zorder=1, )

    ax[2].plot(std_df[word].index,
               std_df[word].rolling(7, center=True, min_periods=3).mean(),
               alpha=0.8,
               lw=2,
               color=color_dict[count_type],
               zorder=1, )

    # plot hedonometer data
    start_date = df.index[0]
    end_date = df.index[-1]
    if hedonometer:
        try:
            df_ref = grab_hedonometer(start_date, end_date, lang=lang)
            happs_index = []
            happs_data = []
            for i,x in df_ref.items():
                happs_index.append(i)
                happs_index.append(i)
                happs_index.append(i)
                happs_data.append(x)
                happs_data.append(np.nan)
                happs_data.append(x)

            ax[1].plot(happs_index, happs_data, '-',
                    color='#9e9e9e',
                    zorder=0,
                    label=f"All Twitter [{lang}]",
                    )
        except KeyError:
            print(f"Language - {lang} is not available")
            pass

    ax[1].plot(df[word].index, df[word],
            zorder=2,
            color=color_dict[count_type],
            label=f'{word} {label_dict[count_type]}',
            drawstyle='steps-post')

    ax[1].xaxis.set_tick_params(rotation=45, labelsize=16)
    ax[1].set_ylim([ylims[0],ylims[1]])
    ax[1].set_ylabel("Ambient Sentiment", fontsize=16)

    if not part:
        locator = mdates.MonthLocator([i * 2 + 1 for i in range(6)])
        formatter = mdates.DateFormatter('%b %Y')
        ax[1].xaxis.set_major_locator(locator)
        ax[1].xaxis.set_major_formatter(formatter)
        ax[0].set_title(word, fontsize=20)
        ax[1].set_ylabel("Ambient Sentiment", fontsize=16)
        ax[1].set_xlabel("Date", fontsize=16)
        ax[1].grid(which='major', axis='x', zorder=-2)
        ax[1].set_axisbelow(True)
        ax[1].legend(fancybox=False)
        plt.tight_layout()
        plt.savefig(f'{pth}sentiment_timeseries_{word}{ngrams}.png', dpi=500)
        plt.savefig(f'{pth}sentiment_timeseries_{word}{ngrams}.pdf')

    return ax

def sentiment_timeseries_plot_simple(df,
                                     count_df,
                                     std_df,
                                     word,
                                     errors=True,
                                     ngrams='1grams',
                                     fig=None,
                                     ylims=(4.5,7.0),
                                     reference=True,
                                     pth="",
                                     **kwargs,
                                     ):
    """ Plot
    """

    print(f'Plotting: {word}')
    part = True # is this function being called as a part of a grid plot?
    if fig is None:
        f, ax = plt.subplots(figsize=(8,5),dpi=400)
        part = False
    else:
        f,ax = fig

    if errors:
        ax.fill_between(std_df[word].index,
                        df[word]-std_df[word]/np.sqrt(count_df[word]/10),
                        df[word]+std_df[word]/np.sqrt(count_df[word]/10),
                        step='post',
                        alpha=0.4,
                        zorder=2,
                        )

        ax.plot(df[word].index, df[word],
                zorder=3,
                lw=3,
                alpha=0.8,
                label=f'{word}',
                drawstyle='steps-post',
                )
    else:

        ax.plot(df[word].index, df[word],
                zorder=1,
                alpha=0.8,
                lw=1,
                label=f'{word}',
                drawstyle='steps-post',
                )

    # plot hedonometer data
    if reference:
        start_date = df.index[0]
        end_date = df.index[-1]
        df_ref = grab_hedonometer(start_date, end_date)
        happs_index = []
        happs_data = []
        for i,x in df_ref.items():
            happs_index.append(i)
            happs_index.append(i)
            happs_index.append(i)
            happs_data.append(x)
            happs_data.append(np.nan)
            happs_data.append(x)

        ax.plot(happs_index, happs_data, '-',
                color='#9e9e9e',
                zorder=0,
                label="All Twitter",
                )

    ax.xaxis.set_tick_params(rotation=45, labelsize=16)
    ax.set_ylim([ylims[0],ylims[1]])
    ax.set_ylabel("Ambient Sentiment", fontsize=16)

    if not part:
        locator = mdates.AutoDateLocator()
        formatter = mdates.AutoDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)
        ax.set_ylabel("Ambient Sentiment", fontsize=16)
        ax.set_xlabel("Date", fontsize=16)
        ax.tick_params(axis='x', labelsize=11, )
        plt.setp(ax.get_xticklabels(), ha='right')
        ax.grid(which='major', axis='x', zorder=-2)
        ax.set_axisbelow(True)
        ax.legend(fancybox=False)
        plt.tight_layout()
        plt.savefig(f'{pth}sentiment_timeseries_{word}{ngrams}.png', dpi=500)
        plt.savefig(f'{pth}sentiment_timeseries_{word}{ngrams}.pdf')

    return ax



def weighted_kde(x, weights, **kwargs):
    """ Wrapper function to get weighted KDE plots"""
    sns.kdeplot(y=x, weights=weights, **kwargs)

def date_label(x, color, label):
    ax = plt.gca()
    ax.text(0.5, 0.0, datetime.strptime(label, '%Y %b %d').strftime('%b %d %Y'),
            fontweight="bold", color=color,
            ha="right", va="center", transform=ax.transAxes,
            rotation=90)

def sentiment_variance_ridge(counters, type_2_score, word):
    """ plot sentiment ridge plots """

    X = []
    Y = []

    num = len(counters.counters)
    for i in range(num):
        try:
            x, y = get_score_distribution(counters.counters[i], type_2_score)
        except TypeError:
            print("Missing counters on a day. Aggregate Further.")
            raise TypeError
        X.append(x)
        Y.append(y)
    df = pd.DataFrame(columns=['X', "g", 'Sentiment'])
    for i in range(num):
        df = df.append(pd.DataFrame(dict(X=X[i],
                                         g=[counters.dates[i].strftime('%Y %b %d')
                                            for j in range(len(X[i]))], Sentiment=Y[i])))
    pal = sns.cubehelix_palette(5, rot=-.25, light=.7)

    g = sns.FacetGrid(df, col="g", hue="g", aspect=0.1, height=7, palette=pal,)
    g.map(weighted_kde, "X", 'Sentiment',
          bw_adjust=.5, clip_on=False,
          fill=True, alpha=1,
          linewidth=1.5,
          )
    g.map(weighted_kde, "X", 'Sentiment',
          clip_on=False, color="w",
          lw=2, bw_adjust=.5,
          )
    g.refline(x=0, linewidth=1, linestyle="-", color=None, clip_on=False)

    # iterate through axes to reverse zordering so distribution line up correctly
    for i, ax in enumerate(g.axes.flat):
        j = ax.get_lines()
        ax.set_zorder(num-i)
        ax.set_facecolor('none')
        for k in j:
            k.set_zorder(2*num-(2*i))
    g.map(date_label, 'X')
    g.set_titles("")
    plt.suptitle(f"{word}", fontsize=25, y=1.00)
    g.set(xticks=[], xlabel="")
    g.despine(bottom=True, left=True, top=True)
    g.figure.subplots_adjust(wspace=-.45)
    plt.show()


def general_sentiment_shift(type2freq_1,
                            type2freq_2,
                            titles=['System 1', 'System 2'],
                            type2score=None,
                            lang='english_v2',
                            stop_lens=[(4,6)],
                            top_n=40,
                            ax=None,
                            ):
    """ General function to plot shifterator shifts"""
    if type2score is None:
        type2score = load_happs_scores(lang=lang)

    reference_value, reference_std = get_weighted_score_np(type2freq_1, type2score)
    sentiment_shift = sh.WeightedAvgShift(type2freq_1=type2freq_1,
                                          type2freq_2=type2freq_2,
                                          type2score_1=type2score,
                                          reference_value=reference_value,
                                          stop_lens=stop_lens,
                                          )

    sentiment_shift.get_shift_graph(ax=ax,
                                    top_n=top_n,
                                    detailed=True,
                                    system_names=titles,
                                    text_size_inset=False,
                                    cumulative_inset=False,
                                    show_plot=False,
                                    )

    return sentiment_shift

def sentiment_shift_dicts(type2freq_1, type2freq_2, lang, titles=None,
                    stop_lens=[(4, 6)]):
    """ Plot sentiment shifts between two distributions """

    type2score = load_happs_scores(lang=lang)

    # filter
    type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
    type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

    if titles is None:
        titles = [f"GeoTweets \n ", f"User Bio Tweets"]

    general_sentiment_shift(type2freq_1, type2freq_2, titles=titles, type2score_1=type2score, top_n=20)

    plt.show()