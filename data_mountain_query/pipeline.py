from data_mountain_query.regexr import get_ngrams_parser, remove_whitespaces, ngrams
from data_mountain_query import counter
from data_mountain_query.parsers import parse_text
from data_mountain_query.sentiment import load_happs_scores, load_vad_scores, load_pds_scores, filter_by_scores, counter_to_dict, counter_sentiment
from data_mountain_query.sentiment_plot import general_sentiment_shift

import matplotlib.pyplot as plt
try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

# load text
# ex: ""


# parsing: storywrangler parser or spacy if stemming
#   Resulting counter object {'burn': 2, 'the': 230}

# example function, but needs edits for
# with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
#     ngrams_parser = get_ngrams_parser(ngrams_pth)
# parse_ngrams_tweets(t, ngrams_parser, scheme)
def tokenize(texts, scheme=1):
    """ Break text into ngram counters"""

    text_counter = counter.TextCounter({})

    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
         ngrams_parser = get_ngrams_parser(ngrams_pth)

    for text in texts:
        text_counter += parse_text(text, ngrams_parser, scheme)

    return text_counter

def ousiometer_score(text_counter, semantic_type='power', language='en'):
    """ Get power/danger/structure scores"""
    word2score = load_vad_scores(semantic_type=semantic_type, language=language)
    return counter_sentiment(text_counter,word2score)

def sentiment_score(text_counter, lang='english_v2'):
    """Get sentiment scores"""
    word2score = load_happs_scores(lang=lang)
    return counter_sentiment(text_counter, word2score)


def main():
    text = ['El niño triste continúa adelante a pesar de su renuencia a soportar el duro trabajo que le espera.']
    text_counter = tokenize(text)
    lang='es'
    print()
    print(f"LabMT sentiment:\n\t {sentiment_score(text_counter, lang='spanish')[0]}")
    print()
    for semantic_type in ['valence', 'arousal', 'dominance']:
        print(f"Ousiometer - {semantic_type} score:\n\t {ousiometer_score(text_counter, semantic_type, language='Spanish')[0]}")
    print()


    text2 = ['El zorro maloliente atacó al asustado ratón de campo y le robó el preciado queso. adelante']
    text_counter2 = tokenize(text2)
    print()
    print(f"LabMT sentiment:\n\t {sentiment_score(text_counter2, 'spanish')[0]:.3f}")
    print()
    for semantic_type in ['valence', 'arousal', 'dominance']:
        print(f"Ousiometer - {semantic_type} score:\n\t {ousiometer_score(text_counter2, semantic_type, 'Spanish')[0]:.3f}")
    print()

    print(text_counter)

    type2freq_1 = counter_to_dict(text_counter, 'count')
    type2freq_2 = counter_to_dict(text_counter2, 'count')

    # use LabMT Sentiment Score
    type2score = load_happs_scores(lang='spanish')

    titles = ['Sentiment \nText 1', 'Text 2']

    general_sentiment_shift(type2freq_1, type2freq_2, type2score=type2score, titles=titles, top_n=15)
    plt.show()

    # use PDS danger scores
    type2score = load_vad_scores('arousal', language='Spanish')

    titles = [' Danger \nText 1', 'Text 2']
    general_sentiment_shift(type2freq_1, type2freq_2, type2score=type2score, titles=titles, top_n=15)
    plt.show()


if __name__ == "__main__":
    main()
