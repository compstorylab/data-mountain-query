import redis

r = redis.Redis(host='localhost', port=6379, decode_responses=True)

res1 = r.hset(
    "bike:1",
    mapping={
        "model": "Deimos",
        "brand": "Ergonom",
        "type": "Enduro bikes",
        "price": 4972,
    },
)

r.hincrby('bike:1', 'price', 100)
