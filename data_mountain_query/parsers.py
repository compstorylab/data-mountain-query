""" Module to store alternative parsers at a higher abstraction level

There are many alternatives to parse text into ngrams.
our storywrangler parser is a good option for
english language tweets, designed to breakout Twitter specific text features like hashtags, user handles and emojis.
However, it prioritizes breadth and accuracy over other potential considerations,
like performance, limiting vocabulary size, etc.



Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py


Todo:
    * Add SpaCy parser
    * WordPeice?
    * standard storywrangler parser

"""
import sys
from pathlib import Path
file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

from multiprocessing import Pool
from data_mountain_query.regexr import get_ngrams_parser, remove_whitespaces, ngrams
from data_mountain_query import counter
import jieba

def load_ngrams_parser():
    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    return ngrams_parser

def preprocess_text_zh(text):
    words = jieba.cut(text)
    # Remove stop words, punctuation, convert to lowercase, etc.
    # Additional preprocessing steps can be added based on your requirements.
    return list(words)

def process_tweet_text(text, rt_flag, ngrams_parser, ngrams, scheme=1):
    """ return counter objects for a tweet text field"""
    t = remove_whitespaces(text)
    tweet_ngrams = ngrams(t, parser=ngrams_parser, n=scheme)
    if tweet_ngrams is None:
        return counter.NgramCounter({})

    # todo: update to include quote and reply text

    if rt_flag == 'retweet':
        tweet_ngrams = counter.NgramCounter(
            {
                k: counter.Storyon(count=v, count_no_rt=0) for k, v in tweet_ngrams.items()
            }
        )
    else:
        tweet_ngrams = counter.NgramCounter(
            {
                k: counter.Storyon(count=v, count_no_rt=v) for k, v in tweet_ngrams.items()
            }
        )
    return tweet_ngrams


def process_reddit_text(text, ngrams_parser, ngrams, scheme=1):
    """ return counter objects for a tweet text field"""
    t = remove_whitespaces(text)
    reddit_ngrams = ngrams(t, parser=ngrams_parser, n=scheme)
    if reddit_ngrams is None:
        return counter.TextCounter({})

    reddit_ngrams = counter.TextCounter(
        {
            k: counter.Texton(count=v) for k, v in reddit_ngrams.items()
        }
    )

    return reddit_ngrams


def parse_ngrams_tweet(tweet, ngrams_parser, scheme=1, lowered=True):
    # to do: currently broken -- rewrite parsing to send "retweet" if retweet
    """ Classify a tweet's retweet type and parse ngrams
    :param t: tweet object
    :param ngrams_parser: regex parser object
    :param scheme: n in ngrams {1, 2, 3, ...}
    """
    ngrams_counter = counter.NgramCounter({})

    # todo: update to include quote and reply text
    flags = ['pure', 'retweet']
    text_types = ['pure_text', 'rt_text']

    for i, field in enumerate(text_types):
        try:
            text = tweet[field]
            if lowered:
                text = text.lower()

            ngrams_counter += process_tweet_text(text, flags[i], ngrams_parser, ngrams, scheme)
        except KeyError:
            pass

    return ngrams_counter


def parse_ngrams_reddit(post, ngrams_parser, scheme=1, lowered=True):
    # to do: currently broken -- rewrite parsing to send "retweet" if retweet
    """ Classify a tweet's retweet type and parse ngrams
    :param t: tweet object
    :param ngrams_parser: regex parser object
    :param scheme: n in ngrams {1, 2, 3, ...}
    """
    ngrams_counter = counter.TextCounter({})

    # todo: update to include quote and reply text
    flags = ['pure', 'retweet']
    text_types = ['pure_text', 'rt_text']


    try:
        text = post['body']
        if lowered:
            text = text.lower()

        ngrams_counter += process_reddit_text(text, ngrams_parser, ngrams, scheme)
    except KeyError:
        pass

    return ngrams_counter


def parse_ngrams_tweet_partial(ngrams_parser, tweet, scheme=1, lowered=True):
    """ Classify a tweet's retweet type and parse ngrams. Modified for multiprocessing
    Args:
        tweet: tweet object
        ngrams_parser: regex parser object
        scheme: n in ngrams {1, 2, 3, ...}
        lowered: bool flag to lower strings

    Returns: Dict {counter: {Counter}, time: {date}}

    """
    ngrams_counter = counter.NgramCounter({})

    # todo: update to include quote and reply text
    flags = ['pure', 'retweet']
    text_types = ['pure_text', 'rt_text']

    for i, field in enumerate(text_types):
        try:
            text = tweet[field]
            if lowered:
                text = text.lower()

            ngrams_counter += process_tweet_text(text, flags[i], ngrams_parser, ngrams, scheme)
        except KeyError:
            pass

    return {'time': tweet['tweet_created_at'], 'counter': ngrams_counter}

def parse_ngrams_reddit_partial(ngrams_parser, comment, scheme=1, lowered=True):
    """ Classify a tweet's retweet type and parse ngrams. Modified for multiprocessing
    Args:
        tweet: tweet object
        ngrams_parser: regex parser object
        scheme: n in ngrams {1, 2, 3, ...}
        lowered: bool flag to lower strings

    Returns: Dict {counter: {Counter}, time: {date}}

    """
    ngrams_counter = counter.TextCounter({})

    # todo: update to include quote and reply text



    try:
        text = comment['body']
        if lowered:
            text = text.lower()

        ngrams_counter += process_reddit_text(text, ngrams_parser, ngrams, scheme)

    except KeyError:
        pass

    return {'time': comment['created_utc'], 'counter': ngrams_counter}

def parse_text(text, ngrams_parser, scheme=1, lowered=True):
    """Simple text parsing for single strings

    Args:
        text (str): string to parse
        ngram_parser: compiled regex
        scheme (int): number of ngrams (words seperated by whitetext)
        lowered (bool): flag to lower text  for uncased parsing

    Returns:
        TextCounter Object
    """

    text_counter = counter.TextCounter({})

    if lowered:
        text = text.lower()

    text_ngrams = ngrams(text, parser=ngrams_parser, n=scheme)

    text_counter += counter.TextCounter(
        { k: counter.Texton(count=v) for k, v in text_ngrams.items()
            }
    )

    return text_counter

def spacy_parser(tweet, scheme, lang):
    """ A SpaCy based parser """
    raise NotImplementedError


def main():
    with pkg_resources.path("pkg_data", f"ngrams.bin") as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)
    print(ngrams_parser)
    print(type(ngrams_parser))


if __name__ == "__main__":
    #main()
    chinese_text = "今天是一个美好的一天，我很高兴。"
    text = preprocess_text_zh(chinese_text)
    print(text)