""" Counter classes to fundamental ngram count data

Basic class methods

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py


Todo:
    * create CounterSeries object here?

"""
from pandas.core.dtypes.dtypes import PandasExtensionDtype
from pandas.api.extensions import ExtensionArray, ExtensionScalarOpsMixin, register_extension_dtype
import numpy as np


class Texton(dict):
    def __init__(self, count=0):
        super().__init__()
        self.update(dict(count=count))

    def __add__(self, other):
        if type(other) == int:
            other = Texton(0)

        self["count"] += other["count"]
        return self

    def __iadd__(self, other):
        if type(other) == int:
            other = Texton(0)

        self["count"] += other["count"]
        return self

    def __repr__(self):
        return f'Texton(count={self["count"]})'

    def extract(self):
        return [self["count"], self["count_no_rt"]]


class Storyon(dict):
    def __init__(self, count=0, count_no_rt=0):
        super().__init__()
        self.update(dict(count=count, count_no_rt=count_no_rt))

    def __repr__(self):
        return f'Storyon(count={self["count"]}, count_no_rt={self["count_no_rt"]})'

    def __gt__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        return self["count"] > other["count"]

    def __lt__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        return self["count"] < other["count"]

    def __eq__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        return self["count"] == other["count"]

    def __add__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        self["count"] += other["count"]
        self["count_no_rt"] += other["count_no_rt"]
        return self

    def __iadd__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        self["count"] += other["count"]
        self["count_no_rt"] += other["count_no_rt"]
        return self

    def extract(self):
        return [self["count"], self["count_no_rt"]]



class ClassMapping(type):
    def __new__(cls, name, bases, dct):
        t = type.__new__(cls, name, bases, dct)
        t._instances = {}
        return t
    def __delitem__(cls, my_str):
        del cls._instances[my_str]

class NgramCounter(dict):
    __metaclass__ = ClassMapping
    def __init__(self, d):
        super(NgramCounter, self).__init__()
        d = {k: Storyon(cc["count"], cc["count_no_rt"]) for k, cc in d.items()}
        self.update(d)

    def __missing__(self, key):
        return Storyon(0, 0)

    def __add__(self, other):
        for k, s in other.items():
            self[k] += s
        return self

    def __iadd__(self, other):
        for k, s in other.items():
            self[k] += s
        return self

    @classmethod
    def _from_sequence(cls, dicts, *, dtype=None, copy=False):
        """
        Construct a new ExtensionArray from a sequence of scalars.
        Each element will be an instance of the scalar type for this array,
        or be converted into this type in this method.
        """
        # Construct new array from sequence of values (Unzip vectors into x and y components)
        #x_values, y_values = zip(*[create_vector(val).as_tuple() for val in scalars])

        return np.array([ NgramCounter(storyon) for storyon in dicts])

class TextCounter(dict):
    """Simpler NgramCounter, without multiple counts"""
    __metaclass__ = ClassMapping

    def __init__(self, d):
        super(TextCounter, self).__init__()
        d = {k: Texton(cc["count"] ) for k, cc in d.items()}
        self.update(d)

    def __missing__(self, key):
        return Texton(0)

    def __add__(self, other):
        for k, s in other.items():
            self[k] += s
        return self

    def __iadd__(self, other):
        for k, s in other.items():
            self[k] += s
        return self


@register_extension_dtype
class CountersDtype(PandasExtensionDtype):
    """
    Class to describe the custom Vector data type
    """
    type = NgramCounter      # Scalar type for data
    name = 'ngram_counter'     # String identifying the data type name

    @classmethod
    def construct_array_type(cls):
        """
        Return array type associated with this dtype
        """
        return NgramCounter

    def __str__(self):
        return self.name


class CounterSeries():
    pass