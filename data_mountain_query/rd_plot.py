import pandas as pd
import data_mountain_query
from data_mountain_query.rank_divergence import ambient_rank_divergence
import subprocess
from datetime import datetime
from multiprocessing import Pool, cpu_count
import logging
import numpy as np
import time


def wrapper():
    with open('states.txt', 'r') as f:
        word_list = [i.strip() for i in f.readlines()]

    args = [('2020', word, '', True) for word in word_list]
    with Pool(6) as p:
        _ = p.starmap(ambient_rank_divergence, args)



def run_rd_figure(word_1,
                  word_2,
                  dates1,
                  dates2,
                  fname1,
                  fname2,
                  matlab,
                  data_pth,
                  tags=''
                  ):
    """ Run a matlab allotax figure script"""
    filename = f'{word_1}_{word_2.replace(" ", "-")}{tags}'
    caption1 = f'{word_1} \\n{dates1[0].strftime("%Y-%m-%d")} to {dates1[-1].strftime("%Y-%m-%d")}'
    caption2 = f'{word_2} \\n{dates2[0].strftime("%Y-%m-%d")} to {dates2[-1].strftime("%Y-%m-%d")}'
    command = f'''{matlab} -r "fig_tweetdb('{fname1.split('/')[0]}', '{fname2.split('/')[0]}', '{data_pth}', '{filename}', '{caption1}', '{caption2}'); exit;"'''
    print(command)
    try:
        subprocess.call(command, shell=True, executable='/bin/zsh')
    except:
        subprocess.call(command, shell=True, executable='/bin/zsh')

    #subprocess.call(['/bin/bash', '-i', '-c', command])  # Todo: remove local path here
    return
'''
Need to convert from tweet_query if still needed
def standard_year_compare(year,
                          anchor,
                          filter=None,
                          high_res=False,
                          caching=False,
                          ngrams='1grams',
                          count_type='count',
                          subsample=True,
                          pth='../data/reference/',
                          data_pth='../data/',
                          matlab='matlab -nosplash -nodesktop',
                          tags='',
                          plot=True,
                          print_head=False,
                          alpha=1/4,
                          lang='en'
                          ):
    """ Compare an ambient distribution with the full twitter year distribution.

    Creates a Rank divergence plot, and returns a pandas dataframe of the most contributing words to the rank divergence from the ambient side

    Filter can be used to return only hashtags, handles or normal words.

    """
    subsample_dict = {
        True: "_subsample",
        False: "",
    }
    
    year_dict = {
        '2019': f'{ngrams}_2019-01-01_2020-01-01_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv',
        '2020': f'{ngrams}_2020-01-01_2021-01-01_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv',
        '2021': f'{ngrams}_2021-01-01_2021-12-31_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv',
        '2022': f'{ngrams}_2022-01-01_2022-05-09_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv'
    }
    date_dict = {
        '2019': (datetime(2019, 1, 1), datetime(2020, 1, 1)),
        '2020': (datetime(2020, 1, 1), datetime(2021, 1, 1)),
        '2021': (datetime(2021, 1, 1), datetime(2021, 12, 31)),
        '2022': (datetime(2022, 1, 1), datetime(2022, 5, 9)),

    }

    start_date = date_dict[year][0]
    end_date = date_dict[year][1]
    dates = pd.date_range(start_date, end_date, freq='D')
    print(f'Grabbing data for {anchor}')
    ambient_counters = data_mountain_query.counters.Counters(dates,
                                                     anchor,
                                                     scheme=int(ngrams[0]),
                                                     lang=lang,
                                                     )
    print(ambient_counters)
    if lang == 'en':
        ambient_counters.get(save=True, caching=False, json=False)
    else:
        ambient_counters.get(save=True, localdb=False, json=True)
    dates = ambient_counters.dates
    ambient_counter = ambient_counters.collapse()
    del ambient_counters

    if caching:
        print('Caching enabled, quitting...')
        return

    ambient_df = pd.DataFrame(ambient_counter).T
    del ambient_counter
    print(f"Finished loading {anchor}")

    reference_df = pd.read_csv(pth + year_dict[year],
                               sep='\t',
                               index_col='ngram')
    print(f"Finished loading Reference")

    if print_head:
        print(ambient_df.head())
        print(ambient_df.shape)

        print(reference_df.head())
        print(reference_df.shape)

    filter_dict = {
        '#':'hashtags',
        '@': "handles",
        'latin':"latin",
    }
    if filter:
        tags=filter_dict[filter]

        if filter == "latin":
            ambient_df = ambient_df[ambient_df.index.str.match('^[a-zA-Z]', na=False)]
            reference_df = reference_df[reference_df.index.str.match('^[a-zA-Z]', na=False)]

        else:
            ambient_df = ambient_df[ambient_df.index.str.startswith(filter, na=False)]
            reference_df = reference_df[reference_df.index.str.startswith(filter, na=False)]

    ambient_df['rank'] = ambient_df[count_type].rank(ascending=False)
    reference_df['rank'] = reference_df[count_type].rank(ascending=False)
    
    if plot:
        fname1 = f'reference_{lang}_{dates[0].strftime("%Y-%m-%d")}_{dates[-1].strftime("%Y-%m-%d")}.tsv'
        reference_df.to_csv(data_pth + fname1, sep="\t")

        fname2 = f'ambient_{lang}_{" ".join(anchor.split("_"))}_{dates[0].strftime("%Y-%m-%d")}_{dates[-1].strftime("%Y-%m-%d")}.tsv'
        ambient_df.to_csv(data_pth + fname2, sep="\t")

        print('Plotting...')
        run_rd_figure('Twitter', anchor, date_dict[year], dates, fname1, fname2, matlab, data_pth, tags=tags+f"_high_res_{high_res}")

        divergence = tweet_query.measurements.rank_divergence(reference_df, ambient_df, alpha=alpha).sort_values(ascending=False)
        return reference_df, ambient_df, divergence

    else:
        divergence = tweet_query.measurements.rank_divergence(reference_df, ambient_df, alpha=alpha).sort_values(ascending=False)
        print(divergence)
        return reference_df, ambient_df, divergence
'''

def rank_divergence_wrapper(ambient_df1, ambient_df2, alpha):
    """ multiprocessing wrapper for creatting divergence matrices"""
    return data_mountain_query.measurements.rank_divergence2(ambient_df1, ambient_df2, alpha=alpha, summed=True)

def divergence_matrix(counters, alpha=1/4, count_type='count', ambient_dfs=None):
    """ Plot a divergence matrix"""
    if ambient_dfs is None:
        ambient_dfs = [pd.DataFrame(ambient_counter).T for ambient_counter in counters.counters]
        for i in range(len(ambient_dfs)):
            ambient_dfs[i]['rank'] = ambient_dfs[i][count_type].rank(ascending=False)
            ambient_dfs[i].sort_index(inplace=True)
    n = len(ambient_dfs)
    divergence = np.zeros((n,n))

    args_list = []
    index_list = []
    for i in range(n):
        for j in range(n):
            if j >=i:
                args_list.append((ambient_dfs[j], ambient_dfs[i], alpha))
                index_list.append((i,j))
    start = time.time()
    with Pool(cpu_count() - 1) as p:
        divs = p.starmap(rank_divergence_wrapper, args_list)

    for args, div_i in zip(index_list, divs):
        divergence[args[0],args[1]] = div_i
        divergence[args[1], args[0]] = div_i
    print(f"{time.time() - start:.2f}s for multiprocessed merge")
    return divergence

if __name__ == '__main__':

    #word = 'Mental_Health'
    #ambient_df = standard_year_compare('2020', word, high_res='Full', filter=None, caching=False, plot=True, ngrams='3grams', subsample=False)
    #print("Done.")
    #word = 'Mental Health'
    #ambient_df = standard_year_compare('2020', word,
    #                                   high_res=False,
    #                                   filter=None, caching=False,
    #                                   plot=True, ngrams='3grams',
    #                                   subsample=True)
    word = 'Russia'


    """
    with open('states.txt', 'r') as f:
        word_list = [i.strip() for i in f.readlines()]
    #word_list = ['Climate Change']
    
    for word in word_list:
        print(word)
        try:
            ambient_df = standard_year_compare('2021', word, high_res=True, filter='#', caching=True, plot=True)
        except ValueError:
            print('removing', word)
            pass
    """
