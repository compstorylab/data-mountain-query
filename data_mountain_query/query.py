"""
Functions for standardized tweet queries

#ToDo: multiprocessed queries by having each interval in the datetimeIndex
    become a dispatch-able job for a worker

"""

import sys
from pathlib import Path

file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

import pandas as pd
import numpy as np
import datetime
import time
from functools import partial
from multiprocessing import Pool
from tqdm import tqdm
import redis
import matplotlib.pyplot as plt

from sklearn.metrics.pairwise import haversine_distances
from data_mountain_query.regexr import get_ngrams_parser
from data_mountain_query.parsers import parse_ngrams_tweet, parse_ngrams_tweet_partial, parse_ngrams_reddit, parse_ngrams_reddit_partial
from data_mountain_query.counter import NgramCounter, Storyon, CountersDtype, Texton, TextCounter
from data_mountain_query.connection import get_connection

def country_query(dates,
               country='US1',
               ):
    """ Function to return a language query dict for pymongo

    Args:
        dates: a pandas DatetimeIndex array
        lang: language to query {'en', 'ar', 'pt', 'zh', etc}

    Returns:
         Query dict
    """

    query = {'tweet_created_at':
                 {'$gte': dates[0],
                  '$lt': dates[-1]
                  },
             'country_label': country,
             }
    return query


def lang_query(dates,
               lang='en',
               ):
    """ Function to return a language query dict for pymongo

    Args:
        dates: a pandas DatetimeIndex array
        lang: language to query {'en', 'ar', 'pt', 'zh', etc}

    Returns:
         Query dict
    """

    query = {'tweet_created_at':
                 {'$gte': dates[0],
                  '$lt': dates[-1]
                  },
             'fastText_lang': lang,
             }
    return query

def get_tweets(collection, query, limit=10_000):
    project = {'pure_text': 1,
              'rt_text': 1,
              'tweet_created_at': 1,
              'id': 1,
              'actor.displayName': 1,
              'fastText_lang': 1,
              'fastText_conf': 1,
              '_id': False,
              'geo.coordinates': 1,
              }

    for t in collection.find(query, project, limit=limit):
        yield t

def get_lang_tweets(collection,
                    dates,
                    lang='en',
                    project=None,
                    limit=10_000):
    """ Wrapper function to execute language queries"""

    if project:
        project = {'pure_text': 1,
                   'rt_text': 1,
                   'tweet_created_at': 1,
                   'id': 1,
                   'actor.displayName': 1,
                   'fastText_lang': 1,
                   'fastText_conf': 1,
                   '_id': False,
                   'geo.coordinates': 1,
                   }

    query = lang_query(dates,
                       lang=lang,
                       )
    print(query)
    for t in collection.find(query, project, limit=limit):
        yield t


def ambient_query(
        word,
        dates,
        lang='en',
        case_sensitive=False,
):
    """ Function to return keyword query dict for pymongo to execute
    Args:
        word: ambient anchor word or n-gram
        dates: a pandas DatetimeIndex array
        lang: language to query
        case_sensitive: query based on case-sensitive keyword

    Returns:
         Query dict

    Read https://docs.mongodb.com/manual/reference/operator/query/text/#mongodb-query-op.-text
    for more on querying with text indexes
    """

    query = {'$text': {'$search': f"\"{word} \"",
                       '$caseSensitive': case_sensitive
                       },
             'tweet_created_at':
                 {'$gte': dates[0],
                  '$lt': dates[-1]
                  },
             'fastText_lang': lang
             }
    return query

def ambient_reddit_query(
        word,
        dates,
        case_sensitive=False,
):
    """ Function to return keyword query dict for pymongo to execute
    Args:
        word: ambient anchor word or n-gram
        dates: a pandas DatetimeIndex array
        lang: language to query
        case_sensitive: query based on case-sensitive keyword

    Returns:
         Query dict

    Read https://docs.mongodb.com/manual/reference/operator/query/text/#mongodb-query-op.-text
    for more on querying with text indexes
    """

    query = {'$text': {'$search': f"\"{word} \"",
                       '$caseSensitive': case_sensitive
                       },
             'created_utc':
                 {'$gte': dates[0],
                  '$lt': dates[-1]
                  },
             }
    return query


def sort_filter_to_counters(tweets,
                            threshold: float,
                            dates: pd.DatetimeIndex,
                            scheme: int = 1,
                            progress_bar: bool = True
                            ) -> object:
    """ parsing for tweets going to different counters with a numeric threshold

    Designed for filtering tweets by location, but maybe useful elsewhere

    Args:
        tweets: list of tweets with "filter_value" field enriched


    """
    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    counters_1 = {i: NgramCounter({}) for i in dates[:-1]}
    counters_series_1 = pd.Series(counters_1, index=dates[:-1], dtype='object')
    counters_2 = {i: NgramCounter({}) for i in dates[:-1]}
    counters_series_2 = pd.Series(counters_2, index=dates[:-1], dtype='object')

    if progress_bar:
        count = len(tweets)
        print(f"Parsing...")
        with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
            for t in tweets:
                date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')
                if t['filter_value'] > threshold:
                    counters_series_1[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)
                else:
                    counters_series_2[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)
                pbar.update()
        print(f"Parsing completed in {time.time() - start_time:.2f}s")
        print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

    else:
        for t in tweets:
            date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')
            if t['filter_value'] > threshold:
                counters_series_1[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)
            else:
                counters_series_2[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)

    return [i for i in counters_series_1.values], [i for i in counters_series_2.values]

def get_ambient_reddit_ngrams(
        word: str,
        dates: pd.DatetimeIndex,
        collection,
        scheme: int = 1,
        case_sensitive: bool = False,
) -> object:
    """ Modular standard parse for reddit ngrams using storywrangler parser

    Args:


    """
    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)
    counters = {i: TextCounter({}) for i in dates[:-1]}

    project = {'body': 1,
               'created_utc': 1,
               }
    query = ambient_reddit_query(word,
                          dates,
                          case_sensitive=case_sensitive,
                          )


    counters_series = pd.Series(counters, index=dates[:-1], dtype='object')
    count = collection.count_documents(query)

    print(f"Querying...")
    with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
        for t in collection.find(query, project):
            date_idx = dates.get_indexer([t['created_utc']], method='pad')
            counters_series.iloc[date_idx] += parse_ngrams_reddit(t, ngrams_parser, scheme)
            pbar.update()
    print(f"Query executed in {time.time() - start_time:.2f}s")
    print(f"Reddit posts processed per second - {count / (time.time() - start_time):.2f} ")

    return [i for i in counters_series.values]


def get_ambient_ngrams(
        word: str,
        dates: pd.DatetimeIndex,
        collection,
        scheme: int = 1,
        lang: str = 'en',
        case_sensitive: bool = False,
        low_mem: bool = True
) -> object:
    """ Modular standard parse for ngrams using storywrangler parser

    Args:


    """
    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)
    counters = {i: NgramCounter({}) for i in dates[:-1]}


    project = {'pure_text': 1,
               'rt_text': 1,
               'tweet_created_at': 1,
               }
    query = ambient_query(word,
                          dates,
                          lang=lang,
                          case_sensitive=case_sensitive,
                          )

    if not low_mem:
        counters_series = pd.Series(counters, index=dates[:-1], dtype='object')

        count = collection.count_documents(query)
        print(f"Querying...")
        with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
            for t in collection.find(query, project):
                date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')
                counters_series.iloc[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)
                pbar.update()
        print(f"Query executed in {time.time() - start_time:.2f}s")
        print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

    else:
        counters_series = pd.Series(counters, index=dates[:-1], dtype='ngram_counter')

        count = collection.count_documents(query)
        print(f"Querying...")
        with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
            for t in collection.find(query, project):
                date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')
                counters_series.iloc[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)
                pbar.update()
        print(f"Query executed in {time.time() - start_time:.2f}s")
        print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")
    print(counters_series.memory_usage(index=True, deep=True))
    return [i for i in counters_series.values]


def get_re_ambient_ngrams(
        word: str,
        dates: pd.DatetimeIndex,
        collection,
        scheme: int = 1,
        lang: str = 'en',
        case_sensitive: bool = False,
        n: int = 8,
):
    """Memory efficient parse using redis to store counters

    """

    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    project = {'pure_text': 1,
               'rt_text': 1,
               'tweet_created_at': 1,
               }
    query = ambient_query(word,
                          dates,
                          lang=lang,
                          case_sensitive=case_sensitive,
                          )

    # todo: redis can only support hashes with integer or string values
    # need to implement separate count and count_no_rt

    # initialize redis connection
    r = redis.Redis(host='localhost', port=6379, decode_responses=True)
    # clear all redis data stores
    r.flushall()

    # create list of redis hashes
    """redis_array = [r.hset(
                        str(i)+text_type,
                        mapping={'.': 0,
                        },
                        )
        for i in range(len(dates[:-1]))
            for text_type in ['_pure', '_all']
    ]"""
    count = collection.count_documents(query)
    with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
        for t in collection.find(query, project):
            counter = parse_ngrams_tweet(t, ngrams_parser, scheme)

            # get date index
            date_index = dates.get_indexer([t['tweet_created_at']], method='pad')
            # add counter to redis array
            [r.hincrby(f"{str(date_index[0])}_all",  ngram, count['count']) for ngram, count in counter.items()]
            [r.hincrby(str(date_index[0]) + "_pure", ngram, count['count_no_rt']) for ngram, count in counter.items()]
            pbar.update()

    # load redis data into counters object
    counters = {i: NgramCounter({}) for i in range(len(dates[:-1]))}
    for i in range(len(dates[:-1])):
        for k,v in r.hgetall(f'{str(i)}_all').items():
            counters[i] += NgramCounter({k: Storyon(count=int(v), count_no_rt=0)})
        for k,v in r.hgetall(f'{str(i)}_pure').items():
            counters[i] += NgramCounter({k: Storyon(count=0, count_no_rt=int(v))})

    print(f"Query + parse + aggregation executed in {time.time() - start_time:.2f}s")
    print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

    return [i for i in counters.values()]



def get_mp_ambient_ngrams(
        word: str,
        dates: pd.DatetimeIndex,
        collection,
        scheme: int = 1,
        lang: str = 'en',
        case_sensitive: bool = False,
        n: int = 8,
        m: int = 4,
) -> object:
    """ Modular standard parse for ngrams using storywrangler parser

    Args:
        word: keyword anchor to query for
        dates: pandas datetime index
        collection: mongodb collection object
        scheme: numbber of ngrams to parse
        lang: fasttext language key to match in query
        case_sensitive: bool flag to lower text
        n: number of cores to use
        m: number of batches per core



    """
    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    project = {'pure_text': 1,
               'rt_text': 1,
               'tweet_created_at': 1,
               }
    query = ambient_query(word,
                          dates,
                          lang=lang,
                          case_sensitive=case_sensitive,
                          )
    parsed = _mp_parse(query, project, ngrams_parser, scheme, collection, n, m)

    # create dataframe
    count = len(parsed)

    df = pd.DataFrame(parsed)
    del parsed  # release memory

    freq = dates.freqstr
    df = df.groupby(df['time'].dt.to_period(freq))['counter'].sum()

    print(f"Query + parse + aggregation executed in {time.time() - start_time:.2f}s")
    print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

    return [i for i in df.values], (count / (time.time() - start_time))

def get_mp_ambient_reddit_ngrams(
        word: str,
        dates: pd.DatetimeIndex,
        collection,
        scheme: int = 1,
        case_sensitive: bool = False,
        n: int = 8,
        m: int = 4,
) -> object:
    """ Modular standard parse for ngrams using storywrangler parser

    Args:
        word: keyword anchor to query for
        dates: pandas datetime index
        collection: mongodb collection object
        scheme: numbber of ngrams to parse
        lang: fasttext language key to match in query
        case_sensitive: bool flag to lower text
        n: number of cores to use
        m: number of batches per core



    """
    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    project = {'body': 1,
               'created_utc': 1,
               }
    query = ambient_reddit_query(word,
                          dates,
                          case_sensitive=case_sensitive,
                          )
    parsed = _mp_reddit_parse(query, project, ngrams_parser, scheme, collection, n, m)

    # create dataframe
    count = len(parsed)

    df = pd.DataFrame(parsed)
    del parsed  # release memory

    freq = dates.freqstr
    df = df.groupby(df['time'].dt.to_period(freq))['counter'].sum()

    print(f"Query + parse + aggregation executed in {time.time() - start_time:.2f}s")
    print(f"Reddit comments processed per second - {count / (time.time() - start_time):.2f} ")

    return [i for i in df.values], (count / (time.time() - start_time))


def _sp_parse(query, project, ngrams_parser, scheme, collection):
    """Single thread query and parse"""


def _mp_parse(query, project, ngrams_parser, scheme, collection, n=8, m=5):
    """ Multiprocessed storywrangler parse for tweets"""

    with Pool(processes=n) as p:
        count = collection.count_documents(query)
        cursor = collection.find(query, project)
        # parse ngrams into {counter: {...}, timestamp: {date}}

        # with 8 cores and chunk size of 40_000, we get 12k to 15k tweets per second
        # with 8 cores 100_000 chunk, we get 12k tweets per second and 58% aquire thread lock time
        parsed = list(tqdm(
            p.imap(
                partial(
                    parse_ngrams_tweet_partial, ngrams_parser, scheme=scheme,
                ),
                cursor,
                chunksize=(count // (m * n)) + 1,
            ), total=count, colour='white', smoothing=0.05
        ))
    return parsed

def _mp_reddit_parse(query, project, ngrams_parser, scheme, collection, n=8, m=5):
    """ Multiprocessed storywrangler parse for tweets"""

    with Pool(processes=n) as p:
        count = collection.count_documents(query)
        cursor = collection.find(query, project)
        # parse ngrams into {counter: {...}, timestamp: {date}}

        # with 8 cores and chunk size of 40_000, we get 12k to 15k tweets per second
        # with 8 cores 100_000 chunk, we get 12k tweets per second and 58% aquire thread lock time
        parsed = list(tqdm(
            p.imap(
                partial(
                    parse_ngrams_reddit_partial, ngrams_parser, scheme=scheme,
                ),
                cursor,
                chunksize=(count // (m * n)) + 1,
            ), total=count, colour='white', smoothing=0.05
        ))
    return parsed


def get_lang_ngrams(dates: pd.DatetimeIndex,
                    collection,
                    scheme: int = 1,
                    lang: str = 'en',
                    progress_bar: bool = True,
                    n: int = 3,
                    mp=False,
                    ) -> object:
    """ Modular standard parse for ngrams using storywrangler parser

    Args:
        dates: a datetime index
        collection:
        scheme:
        lang:
        progress_bar:
        n:
        mp:

    Return: Parsed counters to be appended to counters object


    """
    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    counters = {i: NgramCounter({}) for i in dates[:-1]}
    counters_series = pd.Series(counters, index=dates[:-1], dtype='object')

    project = {'pure_text': 1,
               'rt_text': 1,
               'tweet_created_at': 1,
               }
    query = lang_query(dates,
                       lang=lang,
                       )
    if mp:  # if multiprocessed
        parsed = _mp_parse(query, project, ngrams_parser, scheme, n, collection)
        count = len(parsed)

        df = pd.DataFrame(parsed)
        del parsed  # release memory

        freq = dates.freqstr
        df = df.groupby(df['time'].dt.to_period(freq))['counter'].sum()
        print(f"Query + parse + aggregation executed in {time.time() - start_time:.2f}s")
        print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

        return [i for i in df.values]

    else:  # if not multiprocessed
        if progress_bar:
            count = collection.count_documents(query)
            print(f"Querying - Language: {lang}")
            with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
                for t in collection.find(query, project, no_cursor_timeout=True):
                    date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')

                    counters_series.iloc[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)
                    pbar.update()
            print(f"Query executed in {time.time() - start_time:.2f}s")
            print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

        else:
            for t in collection.find(query, project):
                date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')
                counters_series[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)

        return [i for i in counters_series.values]


def get_country_ngrams(dates: pd.DatetimeIndex,
                    collection,
                    scheme: int = 1,
                    country: str = 'US1',
                    progress_bar: bool = True,
                    n: int = 3,
                    mp=False,
                    ) -> object:
    """ Modular standard parse for ngrams using storywrangler parser to match country label

    Args:
        dates: a datetime index
        collection:
        scheme:
        lang:
        progress_bar:
        n:
        mp:

    Return: Parsed counters to be appended to counters object


    """
    start_time = time.time()

    with pkg_resources.as_file(pkg_resources.files("pkg_data").joinpath(f"ngrams.bin")) as ngrams_pth:
        ngrams_parser = get_ngrams_parser(ngrams_pth)

    counters = {i: NgramCounter({}) for i in dates[:-1]}
    counters_series = pd.Series(counters, index=dates[:-1], dtype='object')

    project = {'pure_text': 1,
               'rt_text': 1,
               'tweet_created_at': 1,
               }
    query = country_query(dates,
                          country=country,
                          )
    if mp:  # if multiprocessed
        parsed = _mp_parse(query, project, ngrams_parser, scheme, n, collection)
        count = len(parsed)

        df = pd.DataFrame(parsed)
        del parsed  # release memory

        freq = dates.freqstr
        df = df.groupby(df['time'].dt.to_period(freq))['counter'].sum()
        print(f"Query + parse + aggregation executed in {time.time() - start_time:.2f}s")
        print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

        return [i for i in df.values]

    else:  # if not multiprocessed
        if progress_bar:
            count = collection.count_documents(query)
            print(f"Querying - Country: {country}")
            with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
                for t in collection.find(query, project, no_cursor_timeout=True):
                    date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')

                    counters_series.iloc[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)
                    pbar.update()
            print(f"Query executed in {time.time() - start_time:.2f}s")
            print(f"Tweets processed per second - {count / (time.time() - start_time):.2f} ")

        else:
            for t in collection.find(query, project):
                date_idx = dates.get_indexer([t['tweet_created_at']], method='pad')
                counters_series[date_idx] += parse_ngrams_tweet(t, ngrams_parser, scheme)

        return [i for i in counters_series.values]


def get_ambient_tweets(
        word,
        dates,
        collection,
        lang='en',
        case_sensitive=False,
        project=None,
):
    """ Standard keyword query to return tweets

    Args:
        word: Keyword to query
        dates:
        collection:
        lang:
        case_sensitive:
        project:

    Yields:
        tweets as type dict

    """

    query = ambient_query(word,
                          dates,
                          lang=lang,
                          case_sensitive=case_sensitive,
                          )
    count = collection.count_documents(query)
    with tqdm(total=count, colour='white', smoothing=0.05) as pbar:
        for t in collection.find(query, project, no_cursor_timeout=True):
            pbar.update()
            yield t


def get_geo_tweets(
        loc,
        dates,
        collection,
        large=True,
        lang='en',
        case_sensitive=False,
        project=None,
):
    """ Standard keyword query to return tweets

    Args:
        loc: Dict with polygon or multipolygon
            ex: {'Polygon'}
        dates:
        collection:
        lang:
        case_sensitive:
        project:

    Yields:
        tweets as type dict

    """
    if large:
        # aggregation with two stages, $geonear at center point, then geowithin

        # find center point and maximum distance for a bounding radius
        max_ll = np.max(loc['coordinates'][0], axis=0)
        min_ll = np.min(loc['coordinates'][0], axis=0)

        max_r = np.radians(max_ll)
        min_r = np.radians(min_ll)

        # distance in meters between two points
        dist = haversine_distances([max_r, min_r]) * 6371000

        # lat 1, long 0
        lat_1 = max_r[1]
        lat_2 = min_r[1]
        long_1 = max_r[0]
        long_2 = min_r[0]
        dlat = np.radians(min_ll[1] - max_ll[1])
        dlong = np.radians(min_ll[0] - max_ll[0])

        Bx = np.cos(lat_2) * np.cos(long_2 - long_1)
        By = np.cos(lat_2) * np.sin(long_2 - long_1)

        lat_m = np.arctan2(np.sin(lat_1) + np.sin(lat_2),
                           np.sqrt((np.cos(lat_1) + Bx) * (np.cos(lat_1) + Bx) + By * By))
        long_m = long_1 + np.arctan2(By, np.cos(lat_1) + Bx)

        center = [long_m * 180 / np.pi, lat_m * 180 / np.pi]
        max_distance = np.sqrt(dist[0, 1] ** 2 + dist[1, 0] ** 2) / 2
        # within bounidng circle
        stage_1 = {'$geoNear': {
            'near': {
                'type': 'Point',
                'coordinates': center
            },
            'distanceField': 'distance',
            'maxDistance': max_distance,
            'query': {
                'tweet_created_at': {'$gte': dates[0],
                                     '$lt': dates[-1]
                                     },
                'fastText_lang': lang,
            },
            'spherical': True
        }
        }

        # within exact polygon
        stage_2 = {'$match':
            {'geo.coordinates': {
                '$geoWithin':
                    {'$geometry': loc},
            }
            }
        }
        pipeline = [stage_1, stage_2]
        for t in collection.aggregate(pipeline=pipeline):
            yield t

    else:

        # simple geowithin
        query = {'geo.coordinates': {
            '$geoWithin':
                {'$geometry':
                     loc
                 },
        },
            'tweet_created_at':
                {'$gte': dates[0],
                 '$lt': dates[-1]
                 },
            'fastText_lang': lang
        }
        for t in collection.find(query, project, no_cursor_timeout=True).hint({'geo': '2dsphere'}):
            yield t

    """
    count = collection.count_documents(query, hint={ 'geo': '2dsphere' })
    print(f"Querying...")
    with tqdm(total=count, colour='white') as pbar:
        for t in collection.find(query, project).hint({ 'geo': '2dsphere' }):
            pbar.update()
            yield t
    """


if "__main__" == __name__:
    start_date = datetime.datetime(2010, 1, 1)
    end_date = datetime.datetime(2023, 4, 1)
    dates = pd.date_range(start_date, end_date, freq='D')

    """loc = {"coordinates": [
        [[-73.33180, 44.99501850852], [-73.299071, 42.7936740],
         [-72.45885564, 42.71355023], [-72.284265, 43.7313056922618],
         [-71.92417284424108, 44.33535869536925], [-71.46587330425345, 44.63119325308577],
         [-71.4549614104437, 45.01816346745454], [-73.33180714563326, 44.99501850852951]],
        [[-74, 45], [-74.4, 45.0], [-74.4, 45.4], [-74, 45]]
    ], "type": "MultiPolygon"}
    # noinspection PyTypeChecker
    print(np.max(loc['coordinates'][0], axis=0))
    # noinspection PyTypeChecker
    print(np.min(loc['coordinates'][0], axis=0))
    collection, client = get_connection(geotweets=True)
    i = get_geo_tweets(loc, dates, collection, large=True)"""
    collection, client = get_connection(p=0.1)

    """x = get_re_ambient_ngrams('Zoning',
                            dates,
                            collection,
                            scheme = 1,
                            lang= 'en',
                            case_sensitive=False,
                            )"""
    """x = get_ambient_ngrams('Zoning',
                            dates,
                            collection,
                            low_mem=False,
                            )

    """

    t_array = np.zeros((9,9))
    m_list = []
    n_list = []
    for m in range(1,10):
        for n in range(1,10,):
            x, count = get_mp_ambient_ngrams('Zoning',
                              dates,
                              collection,
                              scheme=1,
                              lang='en',
                              case_sensitive=False,
                              m=m,
                              n=n,
                              )
            t_array[n-1,m-1] = count


    plt.imshow(t_array)
    plt.show()
