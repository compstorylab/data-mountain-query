import pandas as pd
import datetime
import numpy as np
from sklearn.decomposition import PCA
from math import log
from data_mountain_query.utils import date_index
from tqdm import tqdm
import time
from multiprocessing import Pool, cpu_count



def compute_pca(x1, names):
    names = np.array(names)
    dates = pd.date_range(start=datetime.datetime(2008, 9, 9),
                          end=datetime.datetime(2008, 9, 9) + datetime.timedelta(x1.shape[1]), freq='D')
    # log transform and add offset
    x1 = np.log10(x1 + (10 ** (-8) / 2))

    # compute pca
    pca = PCA()
    obs_trans_ = pca.fit_transform(x1)

    return names, dates, x1, pca, obs_trans_


def rd_timeseries(counters, alpha=1 / 4, freq='D', count_type='count'):
    """ Compute rank divergence of """
    dates = pd.date_range(counters.dates[0], counters.dates[-1], freq=freq)

    #assert dates.freq >= counters.dates.freq, "Provided frequency too high for given data"

    rd_data = np.zeros(len(dates) - 1)
    print("Computing rank divergences...")
    for i in tqdm(range(len(dates) - 1)):
        # make dataframe for each date comparison
        if dates.freq == counters.dates.freq:
            try:
                date_index1 = date_index(dates[i], counters.dates)[0]
                df1 = pd.DataFrame(counters.counters[date_index1]).T

                date_index2 = date_index(dates[i + 1], counters.dates)[0]
                df2 = pd.DataFrame(counters.counters[date_index2]).T
            except IndexError:
                continue

        #elif dates.freq <= counters.dates.freq:
        #    pass

        try:
            df1['rank'] = df1[count_type].rank(ascending=False)
            df2['rank'] = df2[count_type].rank(ascending=False)
            rd_data[i] = rank_divergence2(df1, df2, alpha=alpha, summed=True, normed=True)
        except KeyError:
            rd_data[i] = np.nan
            pass
    return rd_data

def rank_divergence_wrapper(ambient_df1, ambient_df2, alpha):
    """ multiprocessing wrapper for creatting divergence matrices"""
    return rank_divergence2(ambient_df1, ambient_df2, alpha=alpha, summed=True)

def divergence_matrix(counters, alpha=1/4, count_type='count', ambient_dfs=None):
    """ Plot a divergence matrix

    Args:
        counters:
        alpha (float):
        count_type (str):
        ambient_dfs:

    ToDos:
        * add date labels
    """


    if ambient_dfs is None:
        ambient_dfs = [pd.DataFrame(ambient_counter).T for ambient_counter in counters.counters]
        for i in range(len(ambient_dfs)):
            ambient_dfs[i]['rank'] = ambient_dfs[i][count_type].rank(ascending=False)
            ambient_dfs[i].sort_index(inplace=True)
    n = len(ambient_dfs)
    divergence = np.zeros((n,n))

    args_list = []
    index_list = []
    for i in range(n):
        for j in range(n):
            if j >=i:
                args_list.append((ambient_dfs[j], ambient_dfs[i], alpha))
                index_list.append((i,j))
    start = time.time()
    with Pool(cpu_count() - 2) as p:
        divs = p.starmap(rank_divergence_wrapper, args_list)

    for args, div_i in zip(index_list, divs):
        divergence[args[0],args[1]] = div_i
        divergence[args[1], args[0]] = div_i
    print(f"{time.time() - start:.2f}s for multiprocessed merge")
    return divergence

def divergence_matrix_compare(counters1, counters2, alpha=1/4, count_type='count', ambient_dfs=None):
    """ Plot a divergence matrix

    Args:
        counters:
        alpha (float):
        count_type (str):
        ambient_dfs:

    ToDos:
        * add date labels
    """


    if ambient_dfs is None:
        ambient_dfs1 = [pd.DataFrame(ambient_counter).T for ambient_counter in counters1.counters]
        ambient_dfs2 = [pd.DataFrame(ambient_counter).T for ambient_counter in counters2.counters]

        for i in range(len(ambient_dfs1)):
            ambient_dfs1[i]['rank'] = ambient_dfs1[i][count_type].rank(ascending=False)
            ambient_dfs1[i].sort_index(inplace=True)
            ambient_dfs2[i]['rank'] = ambient_dfs2[i][count_type].rank(ascending=False)
            ambient_dfs2[i].sort_index(inplace=True)
    n = len(ambient_dfs1)
    m = len(ambient_dfs2)
    divergence = np.zeros((n,m))

    args_list = []
    index_list = []
    for i in range(n):
        for j in range(m):
            if j >=i:
                args_list.append((ambient_dfs1[j], ambient_dfs2[i], alpha))
                index_list.append((i,j))
    start = time.time()
    with Pool(cpu_count() - 2) as p:
        divs = p.starmap(rank_divergence_wrapper, args_list)

    for args, div_i in zip(index_list, divs):
        divergence[args[0],args[1]] = div_i
        divergence[args[1], args[0]] = div_i
    print(f"{time.time() - start:.2f}s for multiprocessed merge")
    return divergence


def rank_divergence(df1, df2, alpha=1 / 4, summed=False, normed=False):
    """ compute rank divergence on two ambient n-gram dataframes

    Args:
        df1: first dataframe with n-gram ranks
        df2: second dataframe with n-gram ranks
        alpha: rank divergence parameter

    Returns:
        A pandas Series with the computed divergence contributions
        or
        the total divergence as a float
    """

    def divergence(x, y, alpha=1 / 4, normed=False):
        """ Computes divergence betweeen two ranks"""
        sign = np.sign(x - y)
        # prefactor = 1 / ((alpha+1) / alpha) * np.abs((1/x) ** alpha - )
        return sign * ((alpha + 1) / alpha) * np.abs((1 / x) ** alpha - (1 / y) ** alpha) ** (1 / (1 + alpha))

    df = df1.merge(df2, how='outer', suffixes=['_x', '_y'], left_index=True, right_index=True).fillna(
        np.inf)  # on='1gram'
    if summed:
        return np.sum(
            [np.abs(divergence(x, y, alpha, normed)) for x, y in zip(df['rank_x'].values, df['rank_y'].values)])
    else:
        divergence_vector = [divergence(x, y, alpha) for x, y in zip(df['rank_x'].values, df['rank_y'].values)]
        df['divergence'] = divergence_vector
        return df['divergence']


def rank_divergence2(df1, df2, alpha=1 / 4, summed=False, normed=False):
    """ compute rank divergence on two ambient n-gram dataframes

    Args:
        df1: first dataframe with n-gram ranks
        df2: second dataframe with n-gram ranks
        alpha: rank divergence parameter

    Returns:
        A pandas Series with the computed divergence contributions
        or
        the total divergence as a float
    """

    def divergence(row, prefactor, alpha=1 / 4):
        """ Computes divergence betweeen two ranks"""
        x = row['rank_x']
        y = row['rank_y']
        sign = np.sign(x - y)
        return (1 / prefactor) * sign * ((alpha + 1) / alpha) * np.abs((1 / x) ** alpha - (1 / y) ** alpha) ** (
                    1 / (1 + alpha))

    def normalization(row, Nx, Ny, alpha=1 / 4):
        x = row['rank_x']
        y = row['rank_y']
        prefactor = ((alpha + 1) / alpha) * \
                    np.abs((1 / x) ** alpha - (1 / (Nx + (Ny / 2)) ** alpha)) ** (1 / (1 + alpha)) + \
                    ((alpha + 1) / alpha) * \
                    np.abs(((1 / (Ny + (Nx / 2))) ** alpha) - (1 / y) ** alpha) ** (1 / (1 + alpha))
        return prefactor

    lenx = len(df1)
    leny = len(df2)
    df = df1.merge(df2, how='outer', suffixes=['_x', '_y'], left_index=True, right_index=True).fillna(
        np.inf)  # on='1gram'

    # compute normalization
    prefactor = np.sum(df.apply(normalization, args=(lenx, leny, alpha), axis=1))
    divergence_vector = df.apply(divergence, args=(prefactor, alpha), axis=1)
    if summed:
        return np.sum(np.abs(divergence_vector))
    else:
        return divergence_vector


def jsd(df1, df2, summed=False, weight_1=0.5, weight_2=0.5):
    """ compute jsd on two ambient n-gram dataframes

    :param df1: first dataframe with n-gram frequencies
    :param df2: second dataframe with n-gram frequencies
    # ToDo: Is this supposed to work?
    """
    df = df1.merge(df2, how='outer', on='ngram').fillna(0)
    # get_jsd_type_scores(p_1, p_2, m, weight_1, weight_2, base, alpha)
    divergence_vector = [get_jsd_type_scores(p_1, p_2, p_1 * weight_1 + p_2 * weight_2, weight_1, weight_2, 2, 1) for
                         p_1, p_2 in zip(df['freq_x'].values, df['freq_y'].values)]


def get_jsd_scores(type2p_1, type2p_2, weight_1=0.5, weight_2=0.5, base=2, alpha=1):
    """
    Calculates the contribution of the types in two systems to the Jensen-Shannon
    divergence (JSD) between those systems
    Parameters
    ----------
    type2p_1, type2p_2: dict
        Keys are types of a system and values are relative frequencies of those types
    weight_1, weight_2: float
        Relative weights of type2p_1 and type2p_2 when constructing their mixed
        distribution. Should sum to 1
    base: int
        The base for the logarithm when computing entropy
    alpha: float
        The parameter for the generalized Tsallis entropy. Setting `alpha=1`
        recovers the Shannon entropy.
    Returns
    -------
        Keys are types and values are the weights of each type for its contribution
        to the JSD
    """
    type2m = dict()
    type2score_1 = dict()
    type2score_2 = dict()
    types = set(type2p_1.keys()).union(set(type2p_2.keys()))
    for t in types:
        p_1 = type2p_1[t] if t in type2p_1 else 0
        p_2 = type2p_2[t] if t in type2p_2 else 0
        m = weight_1 * p_1 + weight_2 * p_2
        s1, s2 = get_jsd_type_scores(p_1, p_2, m, weight_1, weight_2, base, alpha)

        type2m[t] = m
        type2score_1[t] = s1
        type2score_2[t] = s2

    return type2m, type2score_1, type2score_2


def get_jsd_type_scores(p_1, p_2, m, weight_1, weight_2, base=2, alpha=1):
    """
    Calculates the JSD weighted average scores for a particular type in a system
    Parameters
    ----------
    p_1, p_2, m: float
        the probability of the type appearing in system 1, system 2, and the
        their mixture M
    weight_1, weight_2: float
        relative weights of type2p_1 and type2p_2 when constructing their mixed
        distribution. Should sum to 1
    base: int
        the base for the logarithm when computing entropy
    alpha: float
        the parameter for the generalized Tsallis entropy. Setting `alpha=1`
        recovers the Shannon entropy.
    Returns
    -------
    score_1, score_2: float
        The weights of the type's contribution
    """
    score_1 = 0
    score_2 = 0
    if alpha == 1:
        if p_1 > 0:
            score_1 = weight_1 * (log(m, base) - log(p_1, base))
        else:
            score_1 = weight_1 * log(m, base)
        if p_2 > 0:
            score_2 = weight_2 * (log(p_2, base) - log(m, base))
        else:
            score_2 = weight_2 * -log(m, base)
    elif alpha > 0:
        if p_1 > 0:
            score_1 = weight_1 * (m ** (alpha - 1) - p_1 ** (alpha - 1)) / (alpha - 1)
        if p_2 > 0:
            score_2 = weight_2 * (m ** (alpha - 1) - p_2 ** (alpha - 1)) / (alpha - 1)
    return score_1, score_2
