# -*- coding: utf-8 -*-
""" A command line interface to run ambient queries

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Todo:
    * Create some example use cases
        * Ambient query save to json
        * sentiment plots from ambient query
        * rank divergence from ambient query
    * Update docs

Author: Michael Arnold
"""
