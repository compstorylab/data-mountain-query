# ToDo: Query, saving and loading
import json
from datetime import datetime
from bson import json_util


from data_mountain_query.connection import get_connection
from data_mountain_query.query import get_lang_tweets, get_tweets


class IteratorAsList(list):
    def __init__(self, it):
        self.it = it
    def __iter__(self):
        return self.it
    def __len__(self):
        return 1

def get_lang_sample(dates, p=0.01, lang='en', geotweets=False, returned=False, limit=10_000):
    """ Query for a sample of tweets by language and save to json"""

    collection, client = get_connection(p=p, geotweets=geotweets)

    tweets = []
    with open(f'../data/{lang}_tweets_sample.json', 'w', encoding='utf-8') as f:
        tweets = get_lang_tweets(collection, dates, lang=lang, project=True, limit=limit)

        if returned:
            json.dump(IteratorAsList(tweets), f, default=json_util.default, indent=4)
        else:
            return tweets

def get_sample_flex(query, p=0.01, geotweets=True, limit=10_000):
    """Query for an a"""
    collection, client = get_connection(p=p, geotweets=geotweets)

    return get_tweets(collection, query, limit)

def count_tweets(query, p=0.01):
    """ Count tweets matching a query

    Args:
        query (dict): pymongo query document
        p (float): sample probablity for tweet collection selection {1.0, 0.1, 0.01}

    Returns:
        Number of matching tweets (float)
    """

    collection, client = get_connection(p=p)
    count = collection.count_documents(query)
    client.close()
    return count

def main():
    start_date = datetime(2019,2,1)
    end_date = datetime(2019,3,1)
    print('Connecting')
    get_lang_sample((start_date, end_date), lang='zh')
    print("Done")

if __name__ == "__main__":
    main()