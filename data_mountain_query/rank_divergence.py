import pandas as pd
import subprocess
import os
from datetime import datetime
from multiprocessing import Pool
import data_mountain_query
from data_mountain_query.utils import ambient_ngrams_dataframe_from_counter
from data_mountain_query.measurements import rank_divergence, rank_divergence2

def ambient_rank_divergence(counter_1,
                            counter_2,
                            word_1,
                            word_2,
                            dates1,
                            dates2,
                            plot=True,
                            alpha=1/4,
                            matlab='matlab',
                            data_pth='../data/',
                            count_type='count',
                            filter=None):
    """matlabplot plot of ambient rank divergence for two words in two date ranges

    Args:
        counter_1: first ambient counter object
        counter_2: second ambient counter object
        word_1: first ambient anchor
        word_2: second ambient anchor
        dates1: tuples of datetime objects for word_1
        dates2: tuples of datetime objects for word_2
        matlab: string of matlab command. Change to full path location if necessary, e.g.

        """

    # convert counter to dataframe
    dist_dict1 = ambient_ngrams_dataframe_from_counter(counter_1,
                                                      word_1,
                                                      scheme=1,
                                                      count_type=count_type)

    dist_dict2 = ambient_ngrams_dataframe_from_counter(counter_2,
                                                      word_2,
                                                      scheme=1,
                                                      count_type=count_type)
    if plot:
        fname1 = f'test1_{dates1[0].strftime("%Y-%m-%d")}_{dates1[1].strftime("%Y-%m-%d")}.tsv'
        dist_dict1.to_csv(data_pth + fname1, sep="\t")
        fname2 = f'test2_{dates2[0].strftime("%Y-%m-%d")}_{dates2[1].strftime("%Y-%m-%d")}.tsv'
        dist_dict2.to_csv(data_pth + fname2, sep="\t")
        return run_rd_figure(word_1, word_2, dates1, dates2, fname1, fname2, matlab, data_pth)
    else:
        divergence = rank_divergence2(dist_dict1, dist_dict2, alpha=alpha)
        return divergence



def run_rd_figure(word_1,
                  word_2,
                  dates1,
                  dates2,
                  fname1,
                  fname2,
                  matlab,
                  data_pth,
                  tags=''
                  ):
    """ Run a matlab allotax figure script"""
    filename = f'{word_1}_{word_2.replace(" ", "-")}{tags}'
    caption1 = f'{word_1} \\n{dates1[0].strftime("%Y-%m-%d")} to {dates1[-1].strftime("%Y-%m-%d")}'
    caption2 = f'{word_2} \\n{dates2[0].strftime("%Y-%m-%d")} to {dates2[-1].strftime("%Y-%m-%d")}'
    command = f'''{matlab} -r "fig_tweetdb('{fname1.split('/')[0]}', '{fname2.split('/')[0]}', '{data_pth}', '{filename}', '{caption1}', '{caption2}'); exit;"'''
    print(command)
    print(os.getcwd())
    try:
        subprocess.call(command, shell=True, executable='/bin/zsh')
    except:
        subprocess.call(command, shell=True, executable='/bin/zsh')

    # subprocess.call(['/bin/bash', '-i', '-c', command])  # Todo: remove local path here
    return


def standard_year_compare(year,
                          anchor,
                          filter=None,
                          high_res=False,
                          caching=False,
                          ngrams='1grams',
                          count_type='count',
                          subsample=True,
                          pth='../data/reference/',
                          data_pth='../data/',
                          matlab='matlab -nosplash -nodesktop',
                          tags='',
                          plot=True,
                          print_head=False,
                          alpha=1 / 4,
                          lang='en'
                          ):
    """ Compare an ambient distribution with the full twitter year distribution.

    Creates a Rank divergence plot, and returns a pandas dataframe of the most contributing words to the rank divergence from the ambient side

    Filter can be used to return only hashtags, handles or normal words.

    """
    subsample_dict = {
        True: "_subsample",
        False: "",
    }

    year_dict = {
        '2019': f'{ngrams}_2019-01-01_2020-01-01_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv',
        '2020': f'{ngrams}_2020-01-01_2021-01-01_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv',
        '2021': f'{ngrams}_2021-01-01_2021-12-31_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv',
        '2022': f'{ngrams}_2022-01-01_2022-05-09_freq_<Day>_{lang}{subsample_dict[subsample]}.tsv'
    }
    date_dict = {
        '2019': (datetime(2019, 1, 1), datetime(2020, 1, 1)),
        '2020': (datetime(2020, 1, 1), datetime(2021, 1, 1)),
        '2021': (datetime(2021, 1, 1), datetime(2021, 12, 31)),
        '2022': (datetime(2022, 1, 1), datetime(2022, 5, 9)),

    }

    start_date = date_dict[year][0]
    end_date = date_dict[year][1]
    dates = pd.date_range(start_date, end_date, freq='D')
    print(f'Grabbing data for {anchor}')
    ambient_counters = data_mountain_query.counters.Counters(dates,
                                 anchor,
                                 high_res=high_res,
                                 scheme=int(ngrams[0]),
                                 lang=lang,
                                 )
    print(ambient_counters)
    if lang == 'en':
        ambient_counters.get(save=True, caching=False, json=False)
    else:
        ambient_counters.get(save=True, localdb=False, json=True)
    dates = ambient_counters.dates
    ambient_counter = ambient_counters.collapse()
    del ambient_counters

    if caching:
        print('Caching enabled, quitting...')
        return

    ambient_df = pd.DataFrame(ambient_counter).T
    del ambient_counter
    print(f"Finished loading {anchor}")

    reference_df = pd.read_csv(pth + year_dict[year],
                               sep='\t',
                               index_col='ngram')
    print(f"Finished loading Reference")

    if print_head:
        print(ambient_df.head())
        print(ambient_df.shape)

        print(reference_df.head())
        print(reference_df.shape)

    filter_dict = {
        '#': 'hashtags',
        '@': "handles",
        'latin': "latin",
    }
    if filter:
        tags = filter_dict[filter]

        if filter == "latin":
            ambient_df = ambient_df[ambient_df.index.str.match('^[a-zA-Z]', na=False)]
            reference_df = reference_df[reference_df.index.str.match('^[a-zA-Z]', na=False)]

        else:
            ambient_df = ambient_df[ambient_df.index.str.startswith(filter, na=False)]
            reference_df = reference_df[reference_df.index.str.startswith(filter, na=False)]

    ambient_df['rank'] = ambient_df[count_type].rank(ascending=False)
    reference_df['rank'] = reference_df[count_type].rank(ascending=False)

    if plot:
        fname1 = f'reference_{lang}_{dates[0].strftime("%Y-%m-%d")}_{dates[-1].strftime("%Y-%m-%d")}.tsv'
        reference_df.to_csv(data_pth + fname1, sep="\t")

        fname2 = f'ambient_{lang}_{" ".join(anchor.split("_"))}_{dates[0].strftime("%Y-%m-%d")}_{dates[-1].strftime("%Y-%m-%d")}.tsv'
        ambient_df.to_csv(data_pth + fname2, sep="\t")

        print('Plotting...')
        run_rd_figure('Twitter', anchor, date_dict[year], dates, fname1, fname2, matlab, data_pth,
                      tags=tags + f"_high_res_{high_res}")

        divergence = rank_divergence(reference_df, ambient_df, alpha=alpha).sort_values(
            ascending=False)
        return reference_df, ambient_df, divergence

    else:
        divergence = rank_divergence(reference_df, ambient_df, alpha=alpha).sort_values(
            ascending=False)
        print(divergence)
        return reference_df, ambient_df, divergence