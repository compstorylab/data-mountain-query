# -*- coding: utf-8 -*-
"""Connection file to handle database connections

Contains functions to return pymongo connection objects,
as well as collection objects that can be directly queried.


Todo:
    * Include connection functions for other datasets, such as geotweets, user location tweets, etc.

Author: Michael Arnold
"""

import os, sys
from pathlib import Path
from dotenv import load_dotenv, dotenv_values

file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass
try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

import ujson
import pprint
from pymongo import MongoClient

load_dotenv("~/.data_mountain_query/.env")
USER_NAME = os.getenv('USER_NAME')
PASSWORD = os.getenv('PASSWORD')

def get_credentials():
    """ access credentials """
    #with pkg_resources.open_binary("pkg_data", 'client.json') as f:
    #    credentials = ujson.load(f)
    env = load_dotenv(os.path.expanduser("~/.data_mountain_query/.env"))
    USER_NAME = os.getenv('USER_NAME')
    PASSWORD = os.getenv('PASSWORD')
    return USER_NAME, PASSWORD


def get_tweet_collection(p=0.01):
    """ select collection by sample"""
    samples = {
        0.01: 'driphose2',
        0.1: 'gardenhose',
        1.0: 'decahose',
    }
    try:
        return samples[p]
    except KeyError:
        raise KeyError("Please select a subsample amount from the following float options: {0.01, 0.1, 1.0}")


def db_connect(username=USER_NAME,
               pwd=PASSWORD,
               hostname='bluemoon-mgmt1.uvm.edu',
               port='57017',
               ):
    """ Return pymongo client. Set to connect to localhost on port 57017
    Args:
        username: Username for Data Mountain cluster
        pwd: Password for Data Mountain
        hostname: DataMountain mongos router IP addresses
        port: default port for mongos router connection

    Returns:
        pymongo client object
    """
    if username is None or pwd is None:
        username, pwd = get_credentials()

    # try to connect via the hostname directly
    try:
        try:
            try:
                client = MongoClient(
                    'mongodb://%s:%s@%s:%s' % (username, pwd, hostname, port),
                    serverSelectionTimeoutMS=1000,
                )
                client.server_info()
                print(f"Connecting on {hostname}")

            # fall back to connect over localhost if port forwarding
            except Exception as e:
                client = MongoClient(
                    'mongodb://%s:%s@localhost:%s' % (username, pwd, port),
                    serverSelectionTimeoutMS=1000,
                )
                client.server_info()
                print('Connecting on localhost')

        except Exception as e:
            client = MongoClient(
                'mongodb://%s:%s@dm-mongovm-001:%s' % (username, pwd, port),
                serverSelectionTimeoutMS=1000,
            )
            client.server_info()
            print('Connecting on dm-mongovm-001')
    except:
        raise ConnectionError("Ensure you've updated your mongoDB credentials in .env and "
              "are on the UVM network or connected via VPN.")

    return client



def get_connection(p=0.01, geotweets=False, user_loc=False, reddit=False):
    """ Returns a Data Mountain pymongo collection object for a given subsample of the decahose

    Args:
        p: a float specifying the subsample to select {0.01, 0.1, 1.0}
        geotweets: bool to return geotweets collection
    Returns:
        Pymongo collection object and client
    """
    # get pymongo client
    client = db_connect()

    # choose correct collection
    if geotweets:
        collection = client['tweets']['geotweets']

    elif user_loc:
        collection = client['tweets']['tweet_user_location']

    elif reddit:
        collection = client['reddit']['comments']

    else:
        # select database 'tweets' and collection based on desired subsample p
        collection = client['tweets'][get_tweet_collection(p)]

    return collection, client


def main():
    x = get_connection(0.01)[0]
    print(x)
    pprint.pprint(x.find_one())


if __name__ == "__main__":

    main()

