import sys
from pathlib import Path
file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources
import csv
import requests
import json
import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


lang_dict = {
    'en': 'english_v2',
    'ru': 'russian',
    'de': 'german',
    'es': 'spanish',
    'fr': 'french',
    'ko': 'korean',
    'id': 'indonesian',
    'ps': 'pashto',
    'pt': 'portuguese',
    'ur': 'urdu',
    'uk': '_ukraine_from_russian',
    'ar': 'arabic',
    'ms': 'malay_from_english',
    'fa': 'farsi_from_english',
    'fi': '_finnish_from_english',
    'it': '_italian_from_english',
    'pl': '_polish_from_english',
    'tl': '_tagalog_from_english',
    'nl': '_dutch_from_english',
}

def grab_hedonometer(start_date, end_date, lang='en'):
    """ Download hedonometer sentiment timeseries from hedometer website"""
    uri = f'http://hedonometer.org/api/v1/happiness/?format=json&timeseries__title={lang}_all&date__gte={start_date.strftime("%Y-%m-%d")}&date__lte={end_date.strftime("%Y-%m-%d")}'
    r = requests.get(uri, verify=False)
    df = pd.DataFrame(json.loads(r.content)['objects'])
    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
    df = df.sort_values('date')
    df = df.set_index(pd.DatetimeIndex(df['date']))
    df['happiness'] = pd.to_numeric(df['happiness'])
    df = df.tz_localize('US/Eastern')
    return df['happiness']


def load_vad_scores(semantic_type: str, language: str) -> dict:
    """ Load NRC VAC scores by langauge

    Args:
        semantic_type: A String to specify the timeseries to return.
            Choose from {'Valence', 'Arousal', 'Dominance'}

        language: A capitalizized string specifying language of dictionary to load

    Returns: Dict of word scored indexed by the language in question.
    """
    if language == 'English':
        return load_pds_scores(semantic_type)

    with pkg_resources.path("pkg_data", f"NRC-VAD-{language}-Lexicon.txt") as pds_pth:
        df = pd.read_csv(pds_pth, sep='\t', engine='c', quoting=csv.QUOTE_NONE, )
    column_name = f'{language} Word'
    df = df.set_index(column_name)
    word2score = df[semantic_type.capitalize()].to_dict()

    return word2score


def load_vad_scores_in_english(semantic_type, language):
    """ Load NRC VAC scores by langauge

    Args:
        semantic_type: A String to specify the timeseries to return.
            Choose from {'Valence', 'Arousal', 'Dominance'}

        language: A capitalizized string specifying language of dictionary to load

    Returns: Dict of word scored indexed by the english translation of each word.
    """
    if language == 'English':
        return load_pds_scores(semantic_type)

    with pkg_resources.path("pkg_data", f"NRC-VAD-{language}-Lexicon.txt") as pds_pth:
        df = pd.read_csv(pds_pth, sep='\t', engine='c', quoting=csv.QUOTE_NONE, )
    column_name = f'English Word'
    df = df.set_index(column_name)
    word2score = df[semantic_type.capitalize()].to_dict()

    return word2score

def load_pds_scores(semantic_type):
    """ Load PDS scores

    Args:
        semantic_type: A String to specify the timeseries to return.
            Choose from {'valence', 'arousal', 'dominance',	'goodness',
             'energy', 'structure', 'power', 'danger', 'structure'}
    """
    with pkg_resources.path("pkg_data", f"VAD_GES_PDSdata.txt") as pds_pth:
        df = pd.read_csv(pds_pth, sep='\t', engine='c', quoting=csv.QUOTE_NONE, )
    df = df.set_index('word')
    word2score = df[semantic_type].to_dict()

    return word2score


def load_happs_scores(lang='english_v2', ):
    """Load LabMT Sentiment vectors
    :param lang: Language name to load related scores"""
    with pkg_resources.path("pkg_data", f"labMT2{lang}.txt") as happs_pth:
        happs = pd.read_csv(happs_pth, sep='\t', engine='c', quoting=csv.QUOTE_NONE,)
    try:
        happs = happs.set_index('word')
        word2score = happs['happs'].to_dict()
    except KeyError:
        happs = happs.set_index('Word')
        word2score = happs['Havg'].to_dict()

    return word2score


def get_score_distribution(counter, type2score, count_type='count'):
    """ Calculate the distribution of ambient sentiment contributing to an anchor word's score
    :param counter: A Storyon Counter of the counts of each word
    :param type2score:
    :param count_type:
    :return: tuple of the component scores for a corpus and
    """
    # Check we have a vocabulary to work with
    sum_count = sum(i[count_type] for i in counter.values())
    type2freq = {key: value[count_type] / sum_count for key, value in counter.items()}

    types = set(type2freq.keys()).intersection(set(type2score.keys()))
    if len(types) == 0:
        return
    f_total = sum([freq for t, freq in type2freq.items() if t in types])
    s_weighted = np.array([type2score[t] * freq for t, freq in type2freq.items()
                           if t in types])
    base_scores = [type2score[t] for t, freq in type2freq.items()
                   if t in types]
    return base_scores, s_weighted / f_total


def get_weighted_score_np(type2freq: dict, type2score: dict):
    """
    Calculate the average score of the system specified by the frequencies
    and scores of the types in that system
    Parameters
    ----------
    type2freq: dict
        keys are types and values are frequencies
    type2score: dict
        keys are types and values are scores
    Returns
    -------
    s_avg: float
        Average weighted score of system
    s_std: float
        Standard deviation of score
    """
    # Check we have a vocabulary to work with
    types = set(type2freq.keys()).intersection(set(type2score.keys()))
    if len(types) == 0:
        return None, None
    freq = [freq for t, freq in type2freq.items() if t in types]
    scores = [type2score[t] for t, freq in type2freq.items() if t in types]

    # Get weighted score and std
    try:
        s_avg = np.average(scores, weights=freq)
        variance = np.average((scores - s_avg)**2, weights=freq)
        return s_avg, np.sqrt(variance)
    except ZeroDivisionError:
        return np.nan, np.nan


def counter_to_dict(counter, count_type):
    """ Convert a Counter object to a dictionary of type scores.

    Args:
        counter: An ngram counter object
        count_type: A String to specify the timeseries type.
             Choose from {'count', 'count_no_rt'}
    Returns:
        type2freq: A dictionary of counts
    """
    sum_count = sum(i[count_type] for i in counter.values())
    type2freq = {key: value[count_type] / sum_count for key, value in counter.items()}

    return type2freq


def filter_by_scores(type2freq, type2score, stop_lens):
    """ Loads a dictionary of type scores.

    Args:
        type2freq: dict
            keys are types, values are frequencies of those types
        type2score: dict
            keys are types, values are scores associated with those types
        stop_lens: iterable of 2-tuples
            denotes intervals that should be excluded when calculating shift scores
    Returns:
        type2freq_new: A dict of filtered word frequencies.
        type2score_new: A dict of filtered word scores.
    """
    type2freq_new = dict()
    type2score_new = dict()
    stop_words = set()
    for lower_stop, upper_stop in stop_lens:
        for t in type2score:
            if ((type2score[t] < lower_stop) or (type2score[t] > upper_stop)) \
                    and t not in stop_words:
                try:
                    type2freq_new[t] = type2freq[t]
                except KeyError:
                    pass
                type2score_new[t] = type2score[t]
            else:
                stop_words.add(t)

    return (type2freq_new, type2score_new, stop_words)


def df_sentiment(df, word2score, anchor=None, count_type='count',  stop_lens=[(4, 6)]):
    """ Score a dataframe with a given sentiment dictionary, return score as a float

       Args:
           counter: storyon counters
           word2score: dictionary of labMT scores
           anchor: anchor word to add to stop words
           count_type: count or count_no_rt to include all or only organic counts
           stop_lens: range of labMT words to discard for scoring.

       Returns:
           updated df with a sentiment and std of sentiment columns
       """

    df['sentiment'] = df['counters'].apply(lambda x: counter_sentiment(x, word2score, None)[0])
    df['sentiment_std'] = df['counters'].apply(lambda x: counter_sentiment(x, word2score, None)[2])

    return df

def counter_sentiment(counter, word2score, anchor=None, count_type='count', stop_lens=[(4, 6)]):
    """ Score a Storyons Counter object with a given sentiment dictionary, return score as a float

    Args:
        counter: storyon counters
        word2score: dictionary of labMT scores
        anchor: anchor word to add to stop words
        count_type: count or count_no_rt to include all or only organic counts
        stop_lens: range of labMT words to discard for scoring.

    Returns:
        sentiment scores
        count of n-grams used to score
        std of sentiment
    """

    # remove anchor word
    if anchor is not None:
        try:
            word2score.pop(anchor)
        except KeyError:
            pass
    # compute usage rate
    sum_count = sum(i[count_type] for i in counter.values()) + 1
    word2freq = {key: value[count_type] / sum_count for key, value in counter.items()}

    # apply stop word filter
    word2freq, word2score, stop_words = filter_by_scores(word2freq, word2score, stop_lens)

    # compute number of LabMT words used to compute the sentiment score
    norm_count = sum(value[count_type] for key,value in counter.items() if key in word2score)
    sentiment, std = get_weighted_score_np(word2freq, word2score)

    return sentiment, norm_count, std


def sentiment_timeseries(counters, word2score, anchor=None, count_type='count', stop_lens=[(4, 6)]):
    """ Score a Storyons Counter list with a given sentiment dictionary, return list of scores

    Args:
        counter: A storyon n-gram counter
        word2score: A dictionary of labMT scores
        anchor: An anchor word to add to stop words
        count_type: A count or count_no_rt to include all or only organic counts
        stop_lens: A range of labMT words to discard for scoring.

    Returns:
        A list of scores, and list of # of ngrams used to score"""

    sentiment = []
    ambient_counts = []
    std = []

    # replace counters object if necessary
    if count_type == 'count_rt':
        counters = counters.count_rt()

    for counter in counters:
        sentiment_i, count_i, std_i = counter_sentiment(counter, word2score, anchor, count_type, stop_lens)
        sentiment.append(sentiment_i)
        ambient_counts.append(count_i)
        std.append(std_i)

    return sentiment, ambient_counts, std


def ambient_sentiment_compare(counters1, counters2, titles):
    """ Compare ambient sentiment contributions

    Args:
        counters1: first list of counters to score
        counters2: second list of counters to score
        titles: length 3 list of title strings
    """
    word2score = load_happs_scores(lang='english')

    x1, y1 = get_score_distribution(np.sum(counters1), word2score)
    x2, y2 = get_score_distribution(np.sum(counters2), word2score)

    f = plt.figure(figsize=(12, 12))

    ax1 = plt.subplot(411)
    ax2 = plt.subplot(312)
    ax3 = plt.subplot(414, sharey=ax1)
    ax = [ax1, ax2, ax3]

    n, bins, patches = ax[0].hist(x1, weights=y1, bins=40)
    n1, bins1, patches1 = ax[2].hist(x2, weights=y2, bins=40)
    bins = np.convolve(bins, np.ones((2,)) / 2, mode='valid')
    ax[1].plot(bins, n - n1, 'o-')
    ax[1].axhline(color='k')
    ax[1].set_yscale('symlog', linthreshy=0.0015)
    ax[0].set_yscale('log')
    ax[2].set_yscale('log')
    plt.xlabel("LabMT Sentiment")

    for i in range(3):
        ax[i].set_title(titles[i])
        if i == 1:
            ax[i].text(-0.18, 0.5, "Sentiment \n Contribution \n Difference", ha='center',
                       verticalalignment='center', transform=ax[i].transAxes, fontsize=12, color='black')
            ax[i].text(-0.18, 0.1, "More \n Sentiment \n Contribution \n↓", ha='center',
                       verticalalignment='center', transform=ax[i].transAxes, fontsize=12, color='grey')
            ax[i].text(-0.18, 0.9, "↑ \n More \n Sentiment \n Contribution", ha='center',
                       verticalalignment='center', transform=ax[i].transAxes, fontsize=12, color='grey')
        ax[i].grid(axis='y')
    plt.tight_layout()
    plt.suptitle("Ambient Sentiment Distribution Compare")

def main():
    txt = 'Ok, this is a very positive development'
    x = load_vad_scores('valence', 'Dutch')
    #y = load_happs_scores('Dutch')
    print('VAD')
    print(x)
    print("HAPPY")
    #print(y)
    #analyzer = SentimentIntensityAnalyzer()
    #print(analyzer.polarity_scores(txt))


if __name__ == "__main__":
    main()