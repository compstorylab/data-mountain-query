# -*- coding: utf-8 -*-
""" Utils file to store commonly used general utility functions.

Todo:
    * For module TODOs
    * You have to also use ``sphinx.ext.todo`` extension

Author: Michael Arnold
"""

import argparse
from bson import json_util
import gzip
import json
import datetime
import pandas as pd

def save_json(fname, tweets):
    """ Save tweets to disk as .json.gz"""
    with gzip.open(fname,
                   'wt') as f:
        json.dump(tweets, f, default=json_util.default)


def load_json(fname):
    """ Load a gzipped json object of the tweets"""
    with gzip.open(fname,
                   'rt') as f:
        dict_list = json.load(f, object_hook=json_util.object_hook)
        return dict_list

def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def text_reader(fname):
    with open(fname, 'r') as f:
        word_list = [i.strip() for i in f.readlines()]
    return word_list

def time_tag(time_ranges, time_i):
    """given a time and time series, return a unique timestamp label"""
    for i, date in enumerate(time_ranges):
        if time_i < date:
            break
    return time_ranges[i - 1]

def date_index(date, dates):
    """given a date and a pandas DatetimeIndex, return the index containing said date

    old version
    try:
        index = (dates > date) & (dates <= date + dates.freq)
    except TypeError as e:
        print(f"TypeError: date_index {e}")
        index = (dates > date) & (dates <= date + (dates[1]-dates[0]))
        pass
    return np.argmax(index) - 1
    """
    return dates.get_indexer([date], method='pad')

def ambient_ngrams_dataframe_from_counter(ngrams_counter, word, scheme=1, count_type='count'):
    """ Converts a counter to dataframe
    :param ngrams_counter: ngram counter to convert to dataframe
    :param dates: a pandas date array
    :param cores: number of cores
    :param count_type:
    :return: pandas dataframe of n-gram counts, rank, and frequency
    """

    sorted_grams = sorted(ngrams_counter.items(), key=lambda i: i[1][count_type], reverse=True)
    total_count = sum([i[count_type] for i in ngrams_counter.values()])

    print(f"Total n-grams for {word}: ", total_count)
    df = pd.DataFrame(
        [[x[0], x[1][count_type], x[1][count_type] / total_count if total_count > 0 else 0] for i, x in
         enumerate(sorted_grams)],
        columns=(f'{str(scheme)}gram', count_type, 'freq'))
    df['rank'] = df['freq'].rank(ascending=False)
    df.index = df[f'{str(scheme)}gram']
    return df[[count_type, 'rank', 'freq']]