"""
Counters
---------
An metadata container for time series text data.

Provides a a common way to story and manipulate text data and the output of queries,
and has methods for common plotting functionality to compare text distributions
"""
# todo: add pop_anchor to sentiment plots by removing from sentiment dict
# todo: add title string that's not lowercase
# todo: move general figures into counter's class and just redefine title and label strings for subclasses

import os
import sys
from pathlib import Path

import data_mountain_query.measurements

file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

try:
    sys.path.remove(str(parent))
except ValueError:
    pass

try:
    import importlib.resources as pkg_resources
except ImportError:
    import importlib_resources as pkg_resources

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import datetime, timedelta
import json
import time
import gzip
import matplotlib.dates as mdates

from data_mountain_query.query import get_ambient_ngrams, get_mp_ambient_ngrams, get_ambient_reddit_ngrams, get_mp_ambient_reddit_ngrams
from data_mountain_query.connection import get_connection
from data_mountain_query.counter import NgramCounter, TextCounter
from data_mountain_query.utils import time_tag, date_index
from data_mountain_query.sentiment import load_happs_scores, load_pds_scores, load_vad_scores, counter_to_dict, \
    filter_by_scores, sentiment_timeseries, \
    get_weighted_score_np
from data_mountain_query.sentiment_plot import general_sentiment_shift, sentiment_timeseries_plot, \
    sentiment_timeseries_plot_simple, sentiment_variance_ridge
from data_mountain_query.rank_divergence import ambient_rank_divergence
from data_mountain_query.measurements import divergence_matrix, divergence_matrix_compare
from data_mountain_query.NRC_langs import nrc_lang_dict
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec


rcParams.update({'figure.autolayout': True})

lang_dict = {
    'en': 'english_v2',
    'ru': 'russian',
    'de': 'german',
    'es': 'spanish',
    'fr': 'french',
    'ko': 'korean',
    'id': 'indonesian',
    'ps': 'pashto',
    'pt': 'portuguese',
    'ur': 'urdu',
    'uk': '_ukraine_from_russian',
    'ar': 'arabic',
    'ms': 'malay_from_english',
    'fa': 'farsi_from_english',
    'fi': '_finnish_from_english',
    'it': '_italian_from_english',
    'pl': '_polish_from_english',
    'tl': '_tagalog_from_english',
    'nl': '_dutch_from_english',
}


def plot_formating(axes, counter_len, agg_level, date_length):
    """ shared plot formatting for combined sentiment plots"""
    axsub, ax, ax2 = axes
    if agg_level[-1] == 'D':
        if counter_len < 20:
            locator = mdates.DayLocator(interval=7)
            formatter = mdates.DateFormatter('%b %d')
        elif date_length < timedelta(weeks=4 * 52):
            locator = mdates.MonthLocator([i * 6 + 1 for i in range(2)])
            formatter = mdates.DateFormatter('%b %Y')
        else:
            locator = mdates.MonthLocator([i * 6 + 1 for i in range(1)])
            formatter = mdates.DateFormatter('%b %Y')

    elif agg_level[-1] == 'W':
        if counter_len < 20:
            locator = mdates.MonthLocator([i + 1 for i in range(12)])
        elif date_length < timedelta(weeks=4 * 52):
            locator = mdates.MonthLocator([i * 6 + 1 for i in range(2)])
        else:
            locator = mdates.MonthLocator([i * 2 + 1 for i in range(6)])

        formatter = mdates.DateFormatter('%b %Y')
    elif agg_level[-1] == 'M':
        formatter = mdates.DateFormatter('%b %Y')

        if date_length < timedelta(weeks=52):
            locator = mdates.MonthLocator([i + 1 for i in range(12)])
        elif date_length < timedelta(weeks=4 * 52):
            locator = mdates.MonthLocator([i * 6 + 1 for i in range(2)])
        else:
            locator = mdates.MonthLocator([i * 6 + 1 for i in range(1)])
            formatter = mdates.DateFormatter('%Y')
    else:
        locator = mdates.AutoDateLocator(maxticks=20)
        formatter = mdates.AutoDateFormatter(locator, defaultfmt='%b %d %Y')

    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)
    ax.set_ylabel("Ambient\n Sentiment \n", fontsize=14)
    ax2.set_ylabel("Ambient\n Sentiment \n STD", fontsize=14)
    ax.tick_params(axis='x', labelsize=12)
    plt.setp(ax.get_xticklabels(), ha='right')
    plt.setp(ax2.get_xticklabels(), rotation=45, ha='right')

    return axsub, ax, ax2


def date_range(dates):
    delta = dates[-1] - dates[0]
    return delta


def matching_dates(dates):
    return all(date.equals(dates[0]) for date in dates)


def date_index(date, dates):
    """given a date and a pandas DatetimeIndex, return the index containing said date

    old version
    try:
        index = (dates > date) & (dates <= date + dates.freq)
    except TypeError as e:
        print(f"TypeError: date_index {e}")
        index = (dates > date) & (dates <= date + (dates[1]-dates[0]))
        pass
    return np.argmax(index) - 1
    """
    return dates.get_indexer([date], method='pad')[0]


def extract_timeseries(word, counters, dates, data_type='count'):
    """ extract single pandas series from ngram counter lists
    :param word: target word in ambient words
    :param counter: list of storyon counters
    :param dates: dates range
    :param data_type: return counts for pure or organic text ["count", "count_no_rt", "freq", 'freq_no_rt']
    :return: pandas.Series object for a single target word"""

    if data_type[-5:] == 'no_rt':
        suffix = '_no_rt'
    else:
        suffix = ''

    array = np.zeros(len(counters))

    for i, counter in enumerate(counters):
        if data_type[:5] == 'count':
            try:
                array[i] = counter[word][data_type]
            except KeyError:
                array[i] = 0

        elif data_type[:4] == 'freq':
            total = np.sum(np.fromiter((value['count' + suffix] for value in counter.values()), dtype=float))
            if total > 0:
                try:
                    array[i] = counter[word]['count' + suffix] / total
                except KeyError:
                    array[i] = 0.
            else:
                array[i] = 0.
    return pd.Series(array, index=dates[:-1], name=word)


def top_n_timeseries(counters, dates, N=100, data_type='count'):
    """ extract pandas DataFrame from ngram counter lists
    :param counter: list of storyon counters
    :param dates: dates range
    :param N: number of timeseries to extract
    :param data_type: return counts for pure or organic text ["count", "count_no_rt", "freq", 'freq_no_rt']
    :return: pandas.DataFrame object for top target words"""

    if data_type[-5:] == 'no_rt':
        suffix = 'no_rt'
    else:
        suffix = ''

    total_counter = np.sum(counters, initial=NgramCounter({}))
    sorted_grams = sorted(total_counter.items(), key=lambda i: i[1]['count' + suffix], reverse=True)
    top_words = [i[0] for i in sorted_grams[:N]]

    df = pd.DataFrame(index=dates, columns=top_words)
    for word in top_words:
        df[word] = extract_timeseries(word, counters, dates, data_type).values

    return df


def combine(counters_list, omit_anchor=True):
    """ Given a list of counters, aggregate elementwise, and return a new counter class object

    # ToDo: have this return a new counter object which has a list of anchors as an attribute.
    Args:
        counters_list: A list of counters
    """

    # Test that dates are the same
    dates = [counter_i.dates for counter_i in counters_list]
    assert matching_dates(dates)
    date_index = counters_list[0].dates

    if omit_anchor:
        for i, counter_i in enumerate(counters_list):
            del counters_list[i][counter_i.anchor]

    agg_counters = []

    for d_index, date in enumerate(date_index[:-2]):
        aligned_counters = [ambient_counter_i.counters[
                                ambient_counter_i.dates.get_loc(date)
                            ]
                            for ambient_counter_i in counters_list
                            if (date in ambient_counter_i.dates) and (
                                    len(ambient_counter_i.counters) > ambient_counter_i.dates.get_loc(date))]
        try:
            agg_counters.append(np.sum(aligned_counters,
                                       initial=NgramCounter({})
                                       ))
        except TypeError:
            print("Aligned Counters:")
            print(aligned_counters)

            # if (date in ambient_counter_i.dates) and (len(ambient_counter_i.counters) > ambient_counter_i.dates.get_loc(date))

    counter = Counters(date_index,
                       anchor="",
                       counters=agg_counters,
                       scheme=counters_list[0].scheme,
                       high_res=counters_list[0].high_res
                       )
    counter.freq = date_index.freq
    return counter


class Counters:
    """ Base class to work with array of storyon counter objects.

    Attributes:
            dates: A pandas.DatatimeIndex date index for counters
            counters: A list of n-grams counter objects for each time interval
            scheme: An int specifying what n-grams to parse
    """

    def __init__(self,
                 dates,
                 counters=None,
                 scheme=1,
                 lang='en',
                 anchor=None,

                 ):
        """ Initialize Counters object with essential metadata """
        self.happs_values = None
        self.freq = dates.freq
        self.dates = dates
        self.scheme = scheme
        self.anchor = None
        self.lang = lang
        if counters is not None:
            self.counters = counters
        self.fname = f"../data/general/{self.dates[0].strftime('%Y-%m-%d')}" \
                     f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                     f"_freq_{str(self.dates.freqstr)}_ngrams_{self.scheme}.json"
        return

    def __repr__(self):
        rep = f"Counters object: \n" \
              f"    Dates - {self.dates[0]} to {self.dates[-1]}\n" \
              f"    Scheme - {self.scheme}grams\n" \
              f"    Counters length - {len(self.counters)}\n"
        return rep

    def __delitem__(self, key):
        for i, counter_i in enumerate(self.counters):
            try:
                del self.counters[i][key]
            except KeyError:
                pass

    def save_json(self):
        """ Save a counter object to disk as .json.gz"""
        with gzip.open(self.fname + ".gz",
                       'wt') as f:
            json.dump(self.counters, f)

    def load_json(self):
        """ Load a gzipped json object of the counters"""
        with gzip.open(self.fname + '.gz',
                       'rt') as f:
            dict_list = json.load(f)
            self.counters = [NgramCounter(i)
                             for i in dict_list]

    def load_from_df(self, df_list):
        """ Load a counters object from a list of pandas dataframes

        Args:
            df_list: A list of Pandas DataFrames containing word counts
        """
        self.counters = [NgramCounter(df[['count', 'count_no_rt']].to_dict(orient="index"))
                         for df in df_list]

    def collapse(self, counters=None):
        """ Sum an array of counters and return a single counter"""
        if counters is None:
            counters = self.counters
        collapsed_counter = np.sum(counters, initial=NgramCounter({}))
        return collapsed_counter

    def collapse_range(self, start_date, end_date):
        """ Given a start and end date,
            return a collapsed counter of all coounters within that range

        Args:
              start_date: datetime object to select first counter
              end_date: datetime object to select last counter
        """
        start_index = date_index(start_date, self.dates)
        end_index = date_index(end_date, self.dates)
        counter = np.sum([self.counters[i] for i in range(start_index, end_index + 1)],
                         initial=NgramCounter({})
                         )
        return counter

    def aggregate(self, freq, full=True, inplace=True):
        """ Function to aggregate counters into longer time intervals

        Args:
            freq: freqency to pass to pandas.date_range
                Allowable values of freq:

                {'S', 'T', 'H', 'D', 'W', 'M', 'Y'}
                secondly, minutely, hourly, daily, weekly, monthly and yearly frequency.

                Multiples are also supported, i.e. '12H' for a half daily frequency.

                https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases

            full: Boolean to discard empty ending date range if True,
                Otherwise keep as is.
        """

        dates = pd.date_range(self.dates[0], self.dates[-1], freq=freq)
        old_dates = self.dates
        assert len(dates) > 1, "Selected frequency will collapse to one interval"

        assert (dates[1] - dates[0]) > (old_dates[1] - old_dates[0]), \
            "Must aggregate to a lower frequency"

        assert len(self.counters) <= len(old_dates), "Dates mismatch"

        counters = {i: NgramCounter({}) for i in dates[:-1]}

        c_i = np.zeros(len(dates))

        for i, counter_i in enumerate(self.counters):

            # clip data outside the new date range
            # x > o and
            if old_dates[i] < dates[-1] and old_dates[i] >= dates[0]:
                counters[time_tag(dates, old_dates[i])] += counter_i
                c_i[date_index(old_dates[i], dates)] += 1

        # convert counters from dict to list
        counters = [i for i in counters.values()]

        # cut off last time interval if not full
        if full:
            if c_i[-1] < c_i[0]:
                dates = dates[:-1]
                counters = counters[:-1]

        if inplace:
            self.dates = dates
            self.counters = counters
            self.freq = self.dates.freq

            return
        else:
            return dates

    def date_index(self, date):
        """ returns counter index for a given date"""
        return date_index(date, self.dates)

    def count_rt(self):
        """ returns 'count_rt' list for each counter in a counters list

        By default an n-gram counter stores only
        counts for all tweets and organic tweets,
        this computes the n-gram counts found only in retweets.

        Returns:
            A list of count_rt dictionaries
        """
        count_rt = []
        for i, counter_i in enumerate(self.counters):
            count_rt.append({})
            for key, value in counter_i.items():
                count_rt[i][key] = {'count_rt': value['count'] - value['count_no_rt']}

        return count_rt

    def to_series(self, word, count_type='count'):
        """ Returns a Pandas Series of the count of a single word with a datetime index

            Args:
                word: A string to extract from the counter object
                count_type: A String to specify the timeseries to return.
                Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}

            Returns:
                Pandas Series object with DatetimeIndex
        """
        return extract_timeseries(
            word,
            self.counters,
            self.dates,
            data_type=count_type)

    def to_timeseries_dataframe(self, count_type='count', N=1000):
        """ Returns a Pandas DataFrame of the count of the top N with a datetime index

            Args:
                count_type: A String to specify the timeseries to return.
                Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
                N: Top N n-grams to include.
                    N = -1 to return all. However, this is expensive, and not recommended.

            Returns:
                Pandas Series object with DatetimeIndex
        """
        return top_n_timeseries(
            self.counters,
            self.dates[:-1],
            N=N,
            data_type=count_type)

    def to_zipf_dataframe(self, index):
        """ Returns a Pandas DataFrame of a single counter's word usage rate and the rank

        Args:
            index: An integer to select which counter to convert
        """
        return pd.DataFrame(self.counters[index]).T

    def happs_series(self, anchor=None, count_type='count', lang='en', pop_anchor=True):
        """Returns a Pandas Dataframe of the Sentiment timeseries with a datetime index

        Args:
            anchor: Specifies the anchor word, which is removed from the words used to score sentiment
            count_type: A String to specify the timeseries to return.
            Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}

        Returns:
            A tuple of Pandas DataFrames.
            The first entry is sentiment scores for each date range.
            The second entry is the count of n-grams used to take the measurement.
            The third is the standard error in the sentiment

        Todo: add stop lens support?
        """
        if anchor:
            if self.anchor is None:
                self.anchor = anchor
        try:
            word2score = load_happs_scores(lang=lang_dict[lang])
            if pop_anchor and anchor is not None:
                for component in self.anchor.split(' '):
                    try:
                        word2score.pop(component)
                    except KeyError:
                        pass

            sentiment_array = sentiment_timeseries(self.counters, word2score, anchor=anchor, count_type=count_type)
            self.happs_values, self.happs_counts, self.happs_std = pd.DataFrame(sentiment_array[0],
                                                                                index=self.dates[:len(self.counters)],
                                                                                columns=[anchor]
                                                                                )[anchor], \
                pd.DataFrame(sentiment_array[1],
                             index=self.dates[:len(self.counters)],
                             columns=[anchor]
                             )[anchor], \
                pd.DataFrame(sentiment_array[2],
                             index=self.dates[:len(self.counters)],
                             columns=[anchor]
                             )[anchor]

            return pd.DataFrame(sentiment_array[0],
                                index=self.dates[:len(self.counters)],
                                columns=[anchor]
                                ), \
                pd.DataFrame(sentiment_array[1],
                             index=self.dates[:len(self.counters)],
                             columns=[anchor]
                             ), \
                pd.DataFrame(sentiment_array[2],
                             index=self.dates[:len(self.counters)],
                             columns=[anchor]
                             )
        except KeyError:
            print(f"KeyError: {lang} not in dict.\n Filling with NaN.")
            nan_array = np.zeros(len(self.dates[:len(self.counters)]))
            nan_array[:] = np.nan
            self.happs_values, self.happs_counts, self.happs_std = pd.DataFrame(nan_array,
                                                                                index=self.dates[:len(self.counters)],
                                                                                columns=[anchor]
                                                                                )[anchor], \
                pd.DataFrame(nan_array,
                             index=self.dates[:len(self.counters)],
                             columns=[anchor]
                             )[anchor], \
                pd.DataFrame(nan_array,
                             index=self.dates[:len(self.counters)],
                             columns=[anchor]
                             )[anchor]

    def vad_series(self, anchor, semantic_type, count_type='count', stop_lens=True, pop_anchor=True):
        """Returns a Pandas Dataframe of the VAD timeseries with a datetime index

        Args:
            anchor: Specifies the anchor word, which is removed from the words used to score sentiment
            semantic_type: A String to specify the timeseries to return.
            Choose from {'valence', 'arousal', 'dominance',	'goodness',
             'energy', 'structure', 'power', 'danger', 'structure'}
            count_type: A String to specify the timeseries to return.
            Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}

        Returns:
            A tuple of Pandas DataFrames.
            The first entry is sentiment scores for each date range.
            The second entry is the count of n-grams used to take the measurement.
            The third is the standard error in the sentiment
        """
        if anchor:
            if self.anchor is None:
                self.anchor = anchor

        stop_lens_dict = {'valence': [(-0.0001, 0.0001)],
                          'arousal': [(-0.0001, 0.0001)],
                          'dominance': [(-0.0001, 0.0001)],
                          }

        if stop_lens:  # update default value if provided
            stop_lens = stop_lens_dict[semantic_type]

        word2score = load_vad_scores(semantic_type=semantic_type, language=nrc_lang_dict[self.lang])
        if pop_anchor and self.anchor is not None:
            for component in self.anchor.split(' '):
                try:
                    word2score.pop(component)
                except KeyError:
                    pass

        sentiment_array = sentiment_timeseries(self.counters,
                                               word2score,
                                               anchor=anchor,
                                               count_type=count_type,
                                               stop_lens=stop_lens)
        if not hasattr(self, 'pds_values'):
            self.pds_values, self.pds_counts, self.pds_std = ({}, {}, {})

        s = semantic_type
        self.pds_values[s], self.pds_counts[s], self.pds_std[s] = pd.DataFrame(sentiment_array[0],
                                                                               index=self.dates[:len(self.counters)],
                                                                               columns=[anchor]
                                                                               )[anchor], \
            pd.DataFrame(sentiment_array[1],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )[anchor], \
            pd.DataFrame(sentiment_array[2],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )[anchor]

        return pd.DataFrame(sentiment_array[0],
                            index=self.dates[:len(self.counters)],
                            columns=[anchor]
                            ), \
            pd.DataFrame(sentiment_array[1],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         ), \
            pd.DataFrame(sentiment_array[2],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )

    def pds_series(self, anchor, semantic_type, count_type='count', stop_lens=True, pop_anchor=True):
        """Returns a Pandas Dataframe of the PDS timeseries with a datetime index

        Args:
            anchor: Specifies the anchor word, which is removed from the words used to score sentiment
            semantic_type: A String to specify the timeseries to return.
            Choose from {'valence', 'arousal', 'dominance',	'goodness',
             'energy', 'structure', 'power', 'danger', 'structure'}
            count_type: A String to specify the timeseries to return.
            Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            stop_lens:
            pop_anchor:

        Returns:
            A tuple of Pandas DataFrames.
            The first entry is sentiment scores for each date range.
            The second entry is the count of n-grams used to take the measurement.
            The third is the standard error in the sentiment

        """
        if anchor:
            if self.anchor is None:
                self.anchor = anchor

        stop_lens_dict = {'power': [(-0.001, 0.001)],
                          'danger': [(-0.001, 0.001)],
                          'structure': [(-0.001, 0.001)],
                          'valence': [(-0.001, 0.001)],
                          'arousal': [(-0.001, 0.001)],
                          'dominance': [(-0.001, 0.001)],
                          'goodness': [(-0.001, 0.001)],
                          'energy': [(-0.01, 0.001)],
                          }  # todo: update with data driven defaults

        if stop_lens:  # update default value if provided
            stop_lens = stop_lens_dict[semantic_type]

        word2score = load_pds_scores(semantic_type=semantic_type)
        if pop_anchor and self.anchor is not None:
            for component in self.anchor.split(' '):
                try:
                    word2score.pop(component)
                except KeyError:
                    pass

        sentiment_array = sentiment_timeseries(self.counters,
                                               word2score,
                                               anchor=anchor,
                                               count_type=count_type,
                                               stop_lens=stop_lens)
        if not hasattr(self, 'pds_values'):
            self.pds_values, self.pds_counts, self.pds_std = ({}, {}, {})

        s = semantic_type
        self.pds_values[s], self.pds_counts[s], self.pds_std[s] = pd.DataFrame(sentiment_array[0],
                                                                               index=self.dates[:len(self.counters)],
                                                                               columns=[anchor]
                                                                               )[anchor], \
            pd.DataFrame(sentiment_array[1],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )[anchor], \
            pd.DataFrame(sentiment_array[2],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )[anchor]

        return pd.DataFrame(sentiment_array[0],
                            index=self.dates[:len(self.counters)],
                            columns=[anchor]
                            ), \
            pd.DataFrame(sentiment_array[1],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         ), \
            pd.DataFrame(sentiment_array[2],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )

    '''def vad_series(self, anchor, semantic_type, language, count_type='count', stop_lens=True, pop_anchor=True):
        """Returns a Pandas Dataframe of the VAC timeseries with a datetime index

        Args:
            anchor: Specifies the anchor word, which is removed from the words used to score sentiment
            semantic_type: A String to specify the timeseries to return.
            Choose from {'valence', 'arousal', 'dominance',	'goodness',
             'energy', 'structure', 'power', 'danger', 'structure'}
             language:
            count_type: A String to specify the timeseries to return.
            Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            stop_lens:
            pop_anchor:

        Returns:
            A tuple of Pandas DataFrames.
            The first entry is sentiment scores for each date range.
            The second entry is the count of n-grams used to take the measurement.
            The third is the standard error in the sentiment
        """
        if anchor:
            if self.anchor is None:
                self.anchor = anchor

        stop_lens_dict = {'Valence': [(-0.001, 0.001)],
                          'Arousal': [(-0.001, 0.001)],
                          'Sominance': [(-0.001, 0.001)]
                          }  # todo: update with data driven defaults

        if stop_lens:  # update default value if provided
            stop_lens = stop_lens_dict[semantic_type]


        word2score = load_vad_scores(semantic_type=semantic_type, language=language)
        if pop_anchor and self.anchor is not None:
            for component in self.anchor.split(' '):
                try:
                    word2score.pop(component)
                except KeyError:
                    pass

        sentiment_array = sentiment_timeseries(self.counters,
                                               word2score,
                                               anchor=anchor,
                                               count_type=count_type,
                                               stop_lens=stop_lens)
        if not hasattr(self, 'pds_values'):
            self.pds_values, self.pds_counts, self.pds_std = ({}, {}, {})

        s = semantic_type
        self.pds_values[s], self.pds_counts[s], self.pds_std[s] = pd.DataFrame(sentiment_array[0],
                                                                               index=self.dates[:len(self.counters)],
                                                                               columns=[anchor]
                                                                               )[anchor], \
            pd.DataFrame(sentiment_array[1],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )[anchor], \
            pd.DataFrame(sentiment_array[2],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )[anchor]

        return pd.DataFrame(sentiment_array[0],
                            index=self.dates[:len(self.counters)],
                            columns=[anchor]
                            ), \
            pd.DataFrame(sentiment_array[1],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         ), \
            pd.DataFrame(sentiment_array[2],
                         index=self.dates[:len(self.counters)],
                         columns=[anchor]
                         )'''

    def save_all_series(self, fname):
        """ Save tsv series"""
        df = pd.DataFrame([self.happs_values, self.happs_counts, self.happs_std]).T
        df.index.name = 'date'
        df.columns = ['happs', 'happs_counts', 'happs_std']
        try:
            for semantic_type in self.pds_values.keys():
                df[semantic_type] = self.pds_values[semantic_type]
                df[semantic_type + '_counts'] = self.pds_counts[semantic_type]
                df[semantic_type + '_std'] = self.pds_std[semantic_type]
        except AttributeError:
            print("No PDS values.")
            pass

        df.to_csv(fname, sep='\t')
        return df

    def rd_matrix(self, alpha=1 / 4, count_type='count', date_str='%b %Y'):
        """ plot a self divergence matrix"""

        divergences = divergence_matrix(self, alpha=alpha, count_type=count_type)
        df = pd.DataFrame(divergences,
                          columns=self.dates[:-1].strftime(date_str),
                          index=self.dates[:-1].strftime(date_str)
                          )
        _ = sns.heatmap(df, cmap='magma', vmin=0.7, )
        plt.title(f"{self.title}")
        plt.show()

    def rd_matrix_compare(self, other_counter, alpha=1 / 4, count_type='count', date_str='%Y-%m'):
        """ plot a self divergence matrix"""

        divergences = divergence_matrix_compare(self, other_counter, alpha=alpha, count_type=count_type)
        df = pd.DataFrame(divergences,
                          columns=self.dates[:-1].strftime(date_str),
                          index=self.dates[:-1].strftime(date_str)
                          )
        _ = sns.heatmap(df, cmap='magma', vmin=0.7, )
        plt.title(f"{self.title}")
        plt.show()


class AmbientTweetCounters(Counters):
    """ Counters data class specifically for single language keyword queries"""

    def __init__(self,
                 dates,
                 anchor,
                 counters=None,
                 scheme=1,
                 resolution=0.01,
                 lang='en',
                 case_sensitive=False,
                 pth=None,
                 ):
        """ Initialize Counters object with all metadata needed to reproduce query

        Attributes:
                dates: A pandas.DatatimeIndex date index for counters
                anchor: A str anchor word used to query
                counters: A list of n-grams counter objects for each time interval
                scheme: An int specifying what n-grams to parse
                resolution: A float representing subsample p to search from `driphose` (0.01),
                    `gardenhose` (0.1), or `decahose` (1.0)
                lang: A str FastText language code for initial query
                case_sensitive: A bool to query on a case sensitive anchor
                pth: An optional path for local cache
        """
        super().__init__(dates, counters=counters, scheme=scheme)  # todo: pass variable here?
        if pth is None:
            root_dir = os.path.dirname(os.path.abspath(__file__))
            pth = os.path.join(root_dir, "../data/ambient/")

        self.anchor = anchor
        self.title = anchor + '-Twitter'
        self.resolution = resolution
        self.lang = lang
        self.case_sensitive = case_sensitive

        if case_sensitive is False:
            self.anchor = self.anchor.lower()

        # default save path
        self.fname = f"{pth}{anchor}_{self.dates[0].strftime('%Y-%m-%d')}" \
                     f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                     f"_resolution-{self.resolution}_freq_{str(self.freq)}_lang_{lang}_ngrams_{self.scheme}.json"
        return

    def __repr__(self):
        rep = f"Counters object: \n" \
              f"    Anchor - {self.anchor} \n" \
              f"    Dates - {self.dates[0]} to {self.dates[-1]}\n" \
              f"    Scheme - {self.scheme}grams\n" \
              f"    Lang - {self.lang}\n" \
              f"    Save Path - {self.fname}"
        return rep

    def query(self, p=0.01):
        """ Query the tweet database for new ambient counters"""

        collection, client = get_connection(p=p)

        self.counters = get_ambient_ngrams(f'{self.anchor}',
                                           self.dates,
                                           collection,
                                           self.scheme,
                                           self.lang,
                                           self.case_sensitive
                                           )
        client.close()

    def mp_query(self, p=0.01):
        """ Query the tweet database for new ambient counters"""

        collection, client = get_connection(p=p)

        self.counters, tweets_per_sec = get_mp_ambient_ngrams(f'{self.anchor}',
                                              self.dates,
                                              collection,
                                              self.scheme,
                                              self.lang,
                                              self.case_sensitive
                                              )
        client.close()

    def plot_ambient_timeseries(self, word, ax=None, count_type='freq', logy=True, alone=False, label=None, color=None):
        """ Plot a timeseries of usage within the ambient corpus

        Args:
            word: The word to be plotted and for the Title
            ax: Optional; A matplotlib Axis if this will be part of a larger multi-axis figure
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            logy: A bool to choose to log scale the y axis
        """
        label_dict = {

            'count': "Counts",
            'count_no_rt': "Counts (no retweets)",
            'freq': "Usage Rate",
            'freq_no_rt': "Usage Rate (no retweets)"
        }

        if ax is None:
            alone = True
            f, ax = plt.subplots(1, 1, figsize=(8, 5))
        data = extract_timeseries(word, self.counters,
                                  self.dates,
                                  data_type=count_type
                                  )
        min_attention = np.min(data.values[np.nonzero(data.values)])

        data[data != 0].plot(ax=ax, color=color)

        #ax.plot(data[data == 0].index, data[data == 0] + min_attention / 3, '.', color=ax.get_lines()[-1].get_color(),
        #        ms=1)

        #ax.set_ylabel(label_dict[count_type])
        #ax.set_title(f'Ambient ngram: "{word}"---Anchor ngram: "{self.title}"')

        if logy is True:
            ax.set_yscale("symlog", linthresh=min_attention)

        if alone:
            plt.show()

        return ax

    def plot_sentiment_timeseries_combined(self,
                                           ylims=(5, 7),
                                           unsafe=False,
                                           count_log=True,
                                           lang='en',
                                           save_pth=None,
                                           hedonometer=True,
                                           label='',
                                           color=None,
                                           ):
        """ Plot combined count and count_no_rt sentiment timeseries
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET

        """
        fig = plt.subplots(nrows=3,
                           ncols=1,
                           sharex=True,
                           figsize=(5.5, 7), dpi=500,
                           gridspec_kw={'height_ratios': [1, 3, 2]},
                           layout='constrained',
                           )
        self.plot_sentiment_timeseries(unsafe=True, fig=fig, lang=lang, count_type='count_no_rt',
                                       hedonometer=hedonometer)
        axsub, ax, ax2 = self.plot_sentiment_timeseries(unsafe=True, fig=fig, lang=lang, count_type='count',
                                                        hedonometer=False)

        agg_level = self.dates.freqstr
        counter_len = len(self.counters)
        date_length = date_range(self.dates)

        axsub, ax, ax2 = plot_formating((axsub, ax, ax2), counter_len, agg_level, date_length)

        handles, labels = ax.get_legend_handles_labels()
        ax2.legend(handles, labels, fancybox=False, frameon=False, ncol=3, loc='lower center',
                   bbox_to_anchor=(0.5, -0.65))

        axsub.set_title(self.title, fontsize=20)
        ax.set_axisbelow(True)

        ax.set_ylim(*ylims)

        print(f"Min happs: {self.happs_values.min():.2f}")
        print(f"Max happs: {self.happs_values.max():.2f}")

        if self.happs_values.min() < 5:
            ax.set_ylim(self.happs_values.min() - 0.2, self.happs_values.max() + 0.2)
        fig[0].align_ylabels()
        # plt.subplots_adjust(left=0.2, bottom=0.3,hspace=0.00, wspace=0.00)
        if save_pth:
            figname = f"{save_pth}/{self.anchor}_sentiment_{self.dates[0].strftime('%Y-%m-%d')}_{self.dates[-1].strftime('%Y-%m-%d')}"
            plt.savefig(figname + ".pdf")
            plt.savefig(figname + ".png")

        return axsub, ax, ax2

    def plot_sentiment_timeseries(self,
                                  fig=None,
                                  count_type='count',
                                  ylims=(5, 7.0),
                                  unsafe=False,
                                  count_log=True,
                                  lang='en',
                                  save_pth=None,
                                  hedonometer=True,
                                  label='',
                                  color=None, ):
        """ A method to plot the ambient sentiment timeseries

        Args:
            fig: Optional;  A matplotlib tuple output of plt.subplots (figure, axs)
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            ylims: ylims for sentiment timeseries
            unsafe: flag to allow plotting even when bins have less than 1000 words.
             Don't use unsafe plots for papers!
        """
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET
        df, count_df, std_df = self.happs_series(self.anchor, count_type=count_type, lang=lang)

        if not unsafe:
            assert count_df[self.anchor].median() > 1000, "Aggregate further for meaningful sentiment measurements"

        axs = sentiment_timeseries_plot(df, count_df, std_df, self.anchor,
                                        fig=fig,
                                        ylims=ylims,
                                        count_log=count_log,
                                        lang=lang,
                                        count_type=count_type,
                                        label=label,
                                        color=color,
                                        hedonometer=hedonometer,
                                        )

        if save_pth:
            count_df['sentiment'] = df[self.anchor]
            count_df['std'] = std_df[self.anchor]
            count_df['counts'] = count_df[self.anchor]
            count_df = count_df[['sentiment', 'counts', 'std']]

            fname = f"{self.title}_sentiment_timeseries_{self.dates[0].strftime('%Y-%m-%d')}" \
                    f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                    f"_freq_{str(self.freq)}_lang_{lang}.tsv"
            if type(save_pth) == str:
                fname = save_pth + fname
            else:
                fname = f'../data/sentiment/' + fname
            count_df.to_csv(fname, sep='\t', index=True)

        return axs

    def plot_pds_timeseries_combined(self,
                                     semantic_type='danger',
                                     ylims=(-0.25, 0.25),
                                     reddit=False,
                                     unsafe=False,
                                     count_log=True,
                                     lang='en',
                                     save_pth=None,
                                     label='',
                                     color=None,
                                     ):
        """ Plot combined count and count_no_rt sentiment timeseries
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET

        """
        fig = plt.subplots(nrows=3,
                           ncols=1,
                           sharex=True,
                           figsize=(5.5, 7.5), dpi=300,
                           gridspec_kw={'height_ratios': [1, 3, 2]},
                           layout='constrained',
                           )
        if not reddit:
            self.plot_pds_timeseries(semantic_type, unsafe=True, fig=fig, lang=lang, count_type='count_no_rt', )
        axsub, ax, ax2 = self.plot_pds_timeseries(semantic_type, unsafe=True, fig=fig, lang=lang, count_type='count', )

        agg_level = self.dates.freqstr
        if agg_level[-1] == 'D':
            if len(self.counters) < 20:
                locator = mdates.DayLocator(interval=7)
                formatter = mdates.DateFormatter('%b %d')
            elif date_range(self.dates) < timedelta(weeks=4 * 52):
                locator = mdates.MonthLocator([i * 6 + 1 for i in range(2)])
                formatter = mdates.DateFormatter('%b %Y')
            else:
                locator = mdates.MonthLocator([i * 6 + 1 for i in range(1)])
                formatter = mdates.DateFormatter('%b %Y')

        elif agg_level[-1] == 'W':
            if len(self.counters) < 20:
                locator = mdates.MonthLocator([i + 1 for i in range(12)])
            elif date_range(self.dates) < timedelta(weeks=4 * 52):
                locator = mdates.MonthLocator([i * 6 + 1 for i in range(2)])
            else:
                locator = mdates.MonthLocator([i * 2 + 1 for i in range(6)])

            formatter = mdates.DateFormatter('%b %Y')
        elif agg_level[-1] == 'M':
            formatter = mdates.DateFormatter('%b %Y')

            if date_range(self.dates) < timedelta(weeks=52):
                locator = mdates.MonthLocator([i + 1 for i in range(12)])
            elif date_range(self.dates) < timedelta(weeks=4 * 52):
                locator = mdates.MonthLocator([i * 6 + 1 for i in range(2)])
            else:
                locator = mdates.MonthLocator([i * 6 + 1 for i in range(1)])
                formatter = mdates.DateFormatter('%Y')
        else:
            locator = mdates.AutoDateLocator(maxticks=20)
            formatter = mdates.AutoDateFormatter(locator, defaultfmt='%b %d %Y')

        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)
        ax.set_ylabel(f"Ambient\n {semantic_type.capitalize()}\n", fontsize=14)
        ax2.set_ylabel(f"Ambient\n {semantic_type.capitalize()}\n STD", fontsize=14)
        ax.tick_params(axis='x', labelsize=12)
        plt.setp(ax.get_xticklabels(), ha='right')
        plt.setp(ax2.get_xticklabels(), rotation=45, ha='right')

        # ax.legend(fancybox=False, frameon=False, ncol=3, bbox_to_anchor=(0,-0.05))
        handles, labels = ax.get_legend_handles_labels()
        ax2.legend(handles, labels, fancybox=False, frameon=False, ncol=3, loc='lower center',
                   bbox_to_anchor=(0.5, -.5))

        axsub.set_title(self.title, fontsize=20)
        ax.set_axisbelow(True)

        ax.set_ylim(*ylims)

        # print(f"Min {semantic_type}: {self.happs_values.min():.2f}")
        # print(f"Max {semantic_type}: {self.happs_values.max():.2f}")

        # if self.happs_values.min() < 5:
        #    ax.set_ylim(self.happs_values.min() - 0.2, self.happs_values.max() + 0.2)
        fig[0].align_ylabels()
        # plt.subplots_adjust(left=0.25, bottom=0.3, hspace=0.0, wspace=0.0 )

        if save_pth:
            figname = f"{save_pth}/{self.anchor}_{semantic_type}_{self.dates[0].strftime('%Y-%m-%d')}_{self.dates[-1].strftime('%Y-%m-%d')}"
            plt.savefig(figname + ".pdf")
            plt.savefig(figname + ".png")

        return axsub, ax, ax2

    def plot_pds_timeseries(self,
                            semantic_type,
                            fig=None,
                            count_type='count',
                            ylims=(-0.15, 0.1),
                            unsafe=False,
                            count_log=True,
                            lang='en',
                            save_pth=None,
                            label='',
                            color=None,
                            stop_lens=True):
        """ A method to plot the ambient pds timeseries

        Args:
            semantic_type: {'power', 'danger', 'structure'}
            fig: Optional;  A matplotlib tuple output of plt.subplots (figure, axs)
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            ylims: ylims for sentiment timeseries
            unsafe: flag to allow plotting even when bins have less than 1000 words.
             Don't use unsafe plots for papers!
        """
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET
        df, count_df, std_df = self.pds_series(self.anchor,
                                               semantic_type=semantic_type,
                                               count_type=count_type,
                                               stop_lens=stop_lens,
                                               )
        if not unsafe:
            assert count_df[self.anchor].median() > 1000, "Aggregate further for meaningful sentiment measurements"

        axs = sentiment_timeseries_plot(df, count_df, std_df, self.anchor, fig=fig,
                                        count_log=count_log, lang=lang, ylims=ylims,
                                        count_type=count_type, hedonometer=False, label=label, color=color
                                        )

        # print(df[self.anchor])
        if save_pth:
            count_df[semantic_type] = df[self.anchor]
            count_df['std'] = std_df[self.anchor]
            count_df['counts'] = count_df[self.anchor]
            count_df = count_df[[semantic_type, 'counts', 'std']]

            fname = f"{self.anchor.lower()}_{semantic_type}_timeseries_{self.dates[0].strftime('%Y-%m-%d')}" \
                    f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                    f"_freq_{str(self.freq)}_lang_{lang}.tsv"
            if type(save_pth) == str:
                fname = save_pth + fname
            else:
                fname = f'../data/sentiment/' + fname
            count_df.to_csv(fname, sep='\t', index=True)

        return axs

    def plot_sentiment_timeseries_simple(self,
                                         fig=None,
                                         count_type='count',
                                         ylims=(5, 7.0),
                                         unsafe=False,
                                         errors=True,
                                         **kwargs):
        """ A method to plot the ambient sentiment timeseries

        Args:
            fig: Optional;  A matplotlib tuple output of plt.subplots (figure, axs)
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            ylims: ylims for sentiment timeseries
            unsafe: flag to allow plotting even when bins have less than 1000 words.
             Don't use unsafe plots for papers!
        """
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET
        df, count_df, std_df = self.happs_series(self.anchor, count_type=count_type)

        if not unsafe:
            assert count_df[self.anchor].median() > 1000, "Aggregate further for meaningful sentiment measurements"

        axs = sentiment_timeseries_plot_simple(df, count_df, std_df, self.anchor, fig=fig, ylims=ylims, errors=errors,
                                               **kwargs)

        print(f" Min sentiment: {df[self.anchor].min():.2f}", df.index.values[df[self.anchor].argmin()])
        print(f" Max sentiment: {df[self.anchor].max():.2f}", df.index.values[df[self.anchor].argmax()])

        return axs

    def plot_sentiment_shift_dates(self,
                                   date1,
                                   date2,
                                   count_type='count',
                                   titles=None,
                                   type2score_1='labMT_English',
                                   stop_lens=[(4, 6)],
                                   timezone="UTC",
                                   lang='en',
                                   pop_anchor=True,
                                   ):
        """ Compare two counters within the same counters object, indexed by date

        Args:
            date1: A datetime index for first counter
            date2: A datetime index for second counter
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
        """
        type2score = load_happs_scores(lang=lang_dict[lang])
        """
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        if date2.tzinfo is None:
            date2 = pytz.timezone(pytz_dict['UTC']).localize(date2)
        """
        # look up index by date
        date1_index = date_index(date1, self.dates)
        date2_index = date_index(date2, self.dates)

        # format selected date counters
        type2freq_1 = counter_to_dict(self.counters[date1_index], count_type)
        type2freq_2 = counter_to_dict(self.counters[date2_index], count_type)

        if pop_anchor:
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in self.anchor.split(' '):
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # filter
        type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
        type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        # set default title
        if titles is None:
            titles = [f"Anchor:  {self.title} \n {date1.strftime('%Y-%m-%d')}", f"{date2.strftime('%Y-%m-%d')}"]

        try:
            general_sentiment_shift(type2freq_1, type2freq_2, titles=titles, type2score=type2score,
                                    stop_lens=stop_lens)
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass

    def plot_allotax_shift_dates(
            self,
            date1,
            date2,
            count_type='count',
            titles=None,
            timezone="UTC",
            matlab='matlab -nosplash -nodesktop',
            filter=None,
            pop_anchor=True
    ):
        """ Compare two counters within the same counters object, indexed by date

        Args:
            date1: A datetime index for first counter
            date2: A datetime index for second counter
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
        """
        """
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict[timezone]).localize(date1)
        if date2.tzinfo is None:
            date2 = pytz.timezone(pytz_dict[timezone]).localize(date2)
        """
        # look up index by date
        date1_index = date_index(date1 + timedelta(hours=5), self.dates)
        date2_index = date_index(date2 + timedelta(hours=5), self.dates)

        # format selected date counters
        type2freq_1 = self.counters[date1_index]
        type2freq_2 = self.counters[date2_index]

        if pop_anchor:
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in self.anchor.split(' '):
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # set default title
        if titles is None:
            titles = [f"Anchor:  {self.title} \n {date1.strftime('%Y-%m-%d')}", f"{date2.strftime('%Y-%m-%d')}"]

        print((self.dates[date1_index], self.dates[date1_index + 1]))
        print((self.dates[date2_index], self.dates[date2_index + 1]))
        ambient_rank_divergence(type2freq_1,
                                type2freq_2,
                                self.title,
                                self.title,
                                (self.dates[date1_index], self.dates[date1_index + 1]),
                                (self.dates[date2_index], self.dates[date2_index + 1]),
                                matlab=matlab, )

    def plot_allotax_shift_date_ranges(
            self,
            date_range_1,
            date_range_2,
            other_counters=None,
            count_type='count',
            plot=True,
            titles=None,
            timezone="UTC",
            matlab='matlab -nosplash -nodesktop',
            filter=None,
            pop_anchor=True
    ):
        """ Compare two counters within the same counters object, indexed by date

        Args:
            date_range_1: A datetime index tuple for first counter
            date_range_2: A datetime index for second counter
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            pop_anchor: Bool to remove anchor from measurement lexicon
        """
        """
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict[timezone]).localize(date1)
        if date2.tzinfo is None:
            date2 = pytz.timezone(pytz_dict[timezone]).localize(date2)
        """

        if other_counters is None:
            other_counters = self

        # look up index by date
        date1_indexes = (date_index(date_range_1[0] + timedelta(hours=5), self.dates),
                         date_index(date_range_1[1] + timedelta(hours=5), self.dates))
        date2_indexes = (date_index(date_range_2[0] + timedelta(hours=5), other_counters.dates),
                         date_index(date_range_2[1] + timedelta(hours=5), other_counters.dates))

        # format selected date counters
        type2freq_1 = self.collapse_range(*date_range_1)
        type2freq_2 = other_counters.collapse_range(*date_range_2)

        if pop_anchor:
            anchors = [*self.anchor.split(' '), *other_counters.anchor.split(' ')]
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchors:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # set default title
        if titles is None:
            titles = [
                f"Anchor:  {self.title} \n {self.dates[date1_indexes[0]].strftime('%Y-%m-%d')} - {self.dates[date1_indexes[1] + 1]}",
                f"{other_counters.dates[date2_indexes[0]].strftime('%Y-%m-%d')} - {other_counters.dates[date2_indexes[1] + 1]}"]

        print((self.dates[date1_indexes[0]], self.dates[date1_indexes[1] + 1]))
        print((self.dates[date2_indexes[0]], self.dates[date2_indexes[1] + 1]))
        return ambient_rank_divergence(type2freq_1,
                                       type2freq_2,
                                       self.title,
                                       self.title,
                                       (self.dates[date1_indexes[0]], self.dates[date1_indexes[1] + 1]),
                                       (other_counters.dates[date2_indexes[0]],
                                        other_counters.dates[date2_indexes[1] + 1]),
                                       matlab=matlab,
                                       plot=plot)

    def plot_sentiment_shift_sparklines(self,
                                        date1,
                                        lang = 'en',
                                        score_dict = 'labMT',
                                        count_type = 'count',
                                        titles = None,
                                        range_indexes = None,
                                        timezone = "UTC",
                                        stop_lens = [(4, 6)],
                                        top_n = 30,
                                        ax = None,
                                        pop_anchor = True,
                                        ):

        """ Compare a selected counter within the same counters object to the average, indexed by date
               Args:
                   date1: A datetime index for first counter
                   count_type: A String to specify the timeseries type.
                      Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
                   range_indexes: None or a tuple. e.g. range_indexes=( datetime(2020,1,1), datetime(2021,1,1) )
        """
        if score_dict == 'labMT':
            type2score = load_happs_scores(lang=lang_dict[lang])
        else:
            type2score = load_pds_scores(score_dict)

        """    
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        """

        # look up index by date
        date1_index = date_index(date1, self.dates)
        if range_indexes is not None:
            start_index = date_index(range_indexes[0], self.dates)
            end_index = date_index(range_indexes[0], self.dates)
        else:
            start_index = 0
            end_index = -1

        # format selected date counters
        try:
            type2freq_1 = counter_to_dict(self.collapse(self.counters[start_index: date1_index]), count_type)
            type2freq_2 = counter_to_dict(self.collapse(self.counters[date1_index: end_index]), count_type)
        except TypeError as e:
            print(e)
            print("likely date to split is outside the data range")

        if pop_anchor:
            anchors = [*self.anchor.split(' ')]
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchors:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # filter
        if score_dict == 'labMT':
            type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
            type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        # set default title
        if titles is None:
            if lang == 'en':
                titles = [
                    f"Lexicon: {score_dict.capitalize()} \n Anchor:  {self.title} \n Changepoint: {date1.strftime('%Y-%m-%d')}\n Before",
                    f"After", ]
            else:
                titles = [f"Anchor:  {self.title} [{lang}] \n Changepoint: {date1.strftime('%Y-%m-%d')}\n Before",
                          f"After"]

        try:
            fig = plt.figure(layout="constrained",dpi=200)

            gs = GridSpec(top_n+1, top_n, figure=fig)
            ax1 = fig.add_subplot(gs[:, :(top_n//2)])
            ax1.update(top=1.15)
            axs = [fig.add_subplot(gs[i:i+2, (top_n//2):]) for i in range(top_n)[:-1]]


            shift_graph = general_sentiment_shift(type2freq_1, type2freq_2, titles=titles, type2score=type2score,
                                    stop_lens=stop_lens, top_n=top_n, ax=ax1)

            # get list of top ngrams
            shifts = shift_graph.get_shift_scores()
            top_words = sorted(shifts, key=lambda dict_key: abs(shifts[dict_key]), reverse=True)

            # plot ngram timeseries for each ngram
            for i, ax in enumerate(axs):
                self.plot_ambient_timeseries( top_words[i], ax=axs[i])
                axs[i].axvline(x=date1)

                ax.axis('off')
                pass
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass


    def plot_sentiment_shift_changepoint(self,
                                         date1,
                                         lang='en',
                                         score_dict='labMT',
                                         count_type='count',
                                         titles=None,
                                         range_indexes=None,
                                         timezone="UTC",
                                         stop_lens=[(4, 6)],
                                         top_n=30,
                                         ax=None,
                                         pop_anchor=True,
                                         ):
        """ Compare a selected counter within the same counters object to the average, indexed by date
        Args:
            date1: A datetime index for first counter
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
             range_indexes: None or a tuple. e.g. range_indexes=( datetime(2020,1,1), datetime(2021,1,1) )
        """
        if score_dict == 'labMT':
            type2score = load_happs_scores(lang=lang_dict[lang])
        else:
            type2score = load_pds_scores(score_dict)

        """    
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        """

        # look up index by date
        date1_index = date_index(date1, self.dates)
        if range_indexes is not None:
            start_index = date_index(range_indexes[0], self.dates)
            end_index = date_index(range_indexes[0], self.dates)
        else:
            start_index = 0
            end_index = -1

        # format selected date counters
        try:
            type2freq_1 = counter_to_dict(self.collapse(self.counters[start_index: date1_index]), count_type)
            type2freq_2 = counter_to_dict(self.collapse(self.counters[date1_index: end_index]), count_type)
        except TypeError as e:
            print(e)
            print("likely date to split is outside the data range")

        if pop_anchor:
            anchors = [*self.anchor.split(' ')]
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchors:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # filter
        if score_dict == 'labMT':
            type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
            type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        # set default title
        if titles is None:
            if lang == 'en':
                titles = [
                    f"Lexicon: {score_dict.capitalize()} \n Anchor:  {self.title} \n Changepoint: {date1.strftime('%Y-%m-%d')}\n Before",
                    f"After", ]
            else:
                titles = [f"Anchor:  {self.title} [{lang}] \n Changepoint: {date1.strftime('%Y-%m-%d')}\n Before",
                          f"After"]

        try:
            general_sentiment_shift(type2freq_1, type2freq_2, titles=titles, type2score=type2score,
                                    stop_lens=stop_lens, top_n=top_n, ax=ax)
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass

    def plot_allotax_shift_changepoint(self,
                                       change_date,
                                       count_type='count',
                                       titles=None,
                                       range_indexes=None,
                                       timezone="UTC",
                                       matlab='matlab',
                                       pop_anchor=True,
                                       ):
        """ Compare a selected counter within the same counters object to the average, indexed by date

        Args:
            change_date: A datetime index for first counter
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
             range_indexes: None or a tuple. e.g. range_indexes=( datetime(2020,1,1), datetime(2021,1,1) )
        """
        """ # if we need datetime functionality add back.
        if change_date.tzinfo is None:
            change_date = pytz.timezone(pytz_dict[timezone]).localize(change_date)
        """
        # look up index by date
        date1_index = date_index(change_date, self.dates)
        if range_indexes is not None:
            start_index = date_index(range_indexes[0], self.dates)
            end_index = date_index(range_indexes[1], self.dates)
            start_date, end_date = range_indexes
        else:
            start_index = 0
            end_index = -1
            start_date, end_date = self.dates[0], self.dates[-1]

        # format selected date counters
        type2freq_1 = self.collapse(self.counters[start_index: date1_index])
        type2freq_2 = self.collapse(self.counters[date1_index: end_index])

        if pop_anchor:
            anchors = [*self.anchor.split(' ')]
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchors:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # set default title
        if titles is None:
            titles = [f"Anchor:  {self.title} \n Changepoint: {change_date.strftime('%Y-%m-%d')}\n Before", f"After"]

        ambient_rank_divergence(type2freq_1,
                                type2freq_2,
                                self.title,
                                self.title,
                                (start_date, change_date),
                                (change_date, end_date),
                                matlab=matlab,
                                )

    def plot_sentiment_shift_vs_collapsed(self,
                                          date1,
                                          count_type='count',
                                          titles=None,
                                          stop_lens=[(4, 6)],
                                          ax=None,
                                          top_n=50,
                                          timezone="UTC",
                                          lang='en',
                                          pop_anchor=True,
                                          ):
        """ compare a counter to the entire time range

        Args:

            date1: date1 to select correct counter
        """
        type2score = load_happs_scores(lang=lang_dict[lang])

        """
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        """

        # look up index by date
        date1_index = date_index(date1, self.dates)

        # compare total counter array to selected date
        type2freq_1 = counter_to_dict(self.collapse(), count_type)
        type2freq_2 = counter_to_dict(self.collapse(self.counters[date1_index]), count_type)

        if pop_anchor:
            anchors = [*self.anchor.split(' ')]
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchors:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # filter
        type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
        type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        if titles is None:
            titles = [f"Anchor:  {self.title} \n ", f"{date1.strftime('%Y-%m-%d')}"]

        try:
            sentiment_shift = general_sentiment_shift(type2freq_1,
                                                      type2freq_2,
                                                      titles=titles,
                                                      type2score=type2score,
                                                      stop_lens=stop_lens,
                                                      top_n=top_n,
                                                      ax=ax)
            return sentiment_shift
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass

    def plot_sentiment_shift_collapsed_comparison(self,
                                                  other_counters,
                                                  count_type='count',
                                                  titles=None,
                                                  stop_lens=[(4, 6)],
                                                  ax=None,
                                                  top_n=50,
                                                  timezone="UTC",
                                                  lang='en',
                                                  pop_anchor=True,
                                                  ):
        """ compare a counters object to another provided one, but collapsed

            Args:

                other_counters: another counters object to compare with
            """
        type2score = load_happs_scores(lang=lang_dict[lang])

        """
            if date1.tzinfo is None:
                date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
            """

        # compare total counter array to selected date
        type2freq_1 = counter_to_dict(self.collapse(), count_type)
        type2freq_2 = counter_to_dict(other_counters.collapse(), count_type)

        if pop_anchor:
            anchors = [*self.anchor.split(' '), *other_counters.anchor.split(' ')]
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchors:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # filter
        type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
        type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        if titles is None:
            titles = [f"Anchor:  {self.title} ", f"Anchor: {other_counters.title}"]

        try:
            sentiment_shift = general_sentiment_shift(type2freq_1,
                                                      type2freq_2,
                                                      titles=titles,
                                                      type2score=type2score,
                                                      stop_lens=stop_lens,
                                                      top_n=top_n,
                                                      ax=ax)
            return sentiment_shift
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass

    def plot_pds_shift_collapsed_comparison(self,
                                          other_counters,
                                          count_type='count',
                                          semantic_type='danger',
                                          titles=None,
                                          stop_lens=[(4, 6)],
                                          ax=None,
                                          top_n=50,
                                          timezone="UTC",
                                          lang='en',
                                          pop_anchor=True,
                                          ):
        """ compare a counters object to another provided one, but collapsed

            Args:

                other_counters: another counters object to compare with
            """
        #type2score = load_happs_scores(lang=lang_dict[lang])
        type2score = load_pds_scores(semantic_type=semantic_type)

        """
            if date1.tzinfo is None:
                date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
            """

        # compare total counter array to selected date
        type2freq_1 = counter_to_dict(self.collapse(), count_type)
        type2freq_2 = counter_to_dict(other_counters.collapse(), count_type)

        if pop_anchor:
            anchors = [*self.anchor.split(' '), *other_counters.anchor.split(' ')]
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchors:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        # filter
        type2freq_1, type2score_new1, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)
        type2freq_2, type2score_new2, stop_words = filter_by_scores(type2freq_2, type2score, stop_lens)

        if titles is None:
            titles = [f"Anchor:  {self.title} ", f"Anchor: {other_counters.title}"]

        try:
            sentiment_shift = general_sentiment_shift(type2freq_1,
                                                      type2freq_2,
                                                      titles=titles,
                                                      type2score=type2score,
                                                      stop_lens=stop_lens,
                                                      top_n=top_n,
                                                      ax=ax)
            return sentiment_shift
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass

    def plot_allotax_shift_collapsed_comparison(self,
                                                other_counters,
                                                count_type='count',
                                                titles=None,
                                                timezone="UTC",
                                                lang='en',
                                                pop_anchor=True,
                                                matlab='matlab',
                                                ):
        """ compare a counters object to another provided one, but collapsed

            Args:

                other_counters: another counters object to compare with
            """

        """
            if date1.tzinfo is None:
                date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
            """

        # compare total counter array to selected date
        type2freq_1 = self.collapse()
        type2freq_2 = other_counters.collapse()

        if pop_anchor:
            anchor1 = self.anchor.split(' ')
            anchor2 = other_counters.anchor.split(' ')

            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in anchor1 + anchor2:
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        if titles is None:
            titles = [f"Anchor:  {self.title} ", f"Anchor: {other_counters.title}"]

        try:

            date_delta = self.dates[1] - self.dates[0]
            ambient_rank_divergence(type2freq_1,
                                    type2freq_2,
                                    self.title,
                                    other_counters.title,
                                    (self.dates[0], self.dates[-1]),  # todo: check on dates her
                                    (other_counters.dates[0], other_counters.dates[-1]),
                                    matlab=matlab,
                                    )
            return
        except ZeroDivisionError:
            print('No ngrams counted for this time rage. Please select another counter, or check your dates')
            pass

    def plot_allotax_shift_vs_collapsed(self,
                                        date1,
                                        count_type='count',
                                        titles=None,
                                        ax=None,
                                        top_n=50,
                                        timezone="UTC",
                                        matlab='matlab',
                                        pop_anchor=True,
                                        ):
        """ compare a counter to the entire time range

        Args:

            date1: date1 to select correct counter
        """
        """
        if date1.tzinfo is None:
            date1 = pytz.timezone(pytz_dict['UTC']).localize(date1)
        """
        # look up index by date
        date1_index = date_index(date1, self.dates)

        # compare total counter array to selected date
        type2freq_1 = self.collapse()
        type2freq_2 = self.counters[date1_index]

        if pop_anchor:
            for type2freq_i in [type2freq_1, type2freq_2]:
                for component in self.anchor.split(' '):
                    try:
                        type2freq_i.pop(component)
                    except KeyError:
                        pass

        if titles is None:
            titles = [f"Anchor:  {self.title} \n ", f"{date1.strftime('%Y-%m-%d')}"]

        date_delta = self.dates[1] - self.dates[0]
        ambient_rank_divergence(type2freq_1,
                                type2freq_2,
                                self.title,
                                self.title,
                                (self.dates[0], self.dates[-1]),
                                (date1, date1 + date_delta),
                                matlab=matlab,
                                )

    def plot_sentiment_ridge(self, stop_lens=[(4, 6)], lens=False):
        """


        """
        type2score = load_happs_scores()

        if lens:
            type2freq_1 = counter_to_dict(self.collapse(), 'count')
            type2freq_1, type2score, stop_words = filter_by_scores(type2freq_1, type2score, stop_lens)

        sentiment_variance_ridge(self, type2score, self.anchor)


class AmbientRedditCounters(AmbientTweetCounters):
    """ Counters data class for Reddit keyword queries"""

    def __init__(self,
                 dates,
                 anchor,
                 counters=None,
                 scheme=1,
                 case_sensitive=False,
                 pth=None,
                 ):
        """ Initialize Counters object with all metadata needed to reproduce query

                Attributes:
                        dates: A pandas.DatatimeIndex date index for counters
                        anchor: A str anchor word used to query
                        counters: A list of n-grams counter objects for each time interval
                        scheme: An int specifying what n-grams to parse
                        case_sensitive: A bool to query on a case sensitive anchor
                        pth: An optional path for local cache
                """
        super().__init__(dates, counters=counters, anchor=anchor, scheme=scheme)
        if pth is None:
            root_dir = os.path.dirname(os.path.abspath(__file__))
            pth = os.path.join(root_dir, "../data/ambient_reddit/")

        self.anchor = anchor
        self.title = anchor + '-Reddit'
        self.case_sensitive = case_sensitive

        if case_sensitive is False:
            self.anchor = self.anchor.lower()

        # default save path
        self.fname = f"{pth}{anchor}_{self.dates[0].strftime('%Y-%m-%d')}" \
                     f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                     f"_freq_{str(self.freq)}_ngrams_{self.scheme}.json"
        return

    def __repr__(self):
        rep = f"Reddit counters object: \n" \
              f"    Anchor - {self.anchor} \n" \
              f"    Dates - {self.dates[0]} to {self.dates[-1]}\n" \
              f"    Scheme - {self.scheme}grams\n" \
              f"    Save Path - {self.fname}"
        return rep

    def query(self):
        """ Query the reddit database for new ambient counters"""

        collection, client = get_connection(reddit=True)

        self.counters = get_ambient_reddit_ngrams(f'{self.anchor}',
                                                  self.dates,
                                                  collection,
                                                  self.scheme,
                                                  self.case_sensitive
                                                  )
        client.close()
        return

    def mp_query(self, p=0.01):
        """ Query the tweet database for new ambient counters"""

        collection, client = get_connection(reddit=True)

        self.counters, comments_per_sec = get_mp_ambient_reddit_ngrams(f'{self.anchor}',
                                                                       self.dates,
                                                                       collection,
                                                                       self.scheme,
                                                                       self.case_sensitive
                                                                       )
        client.close()

    def collapse(self, counters=None):
        """ Sum an array of counters and return a single counter"""
        if counters is None:
            counters = self.counters
        collapsed_counter = np.sum(counters, initial=TextCounter({}))
        return collapsed_counter
    def aggregate(self, freq, full=True, inplace=True):
        """ Function to aggregate counters into longer time intervals

        Args:
            freq: freqency to pass to pandas.date_range
                Allowable values of freq:

                {'S', 'T', 'H', 'D', 'W', 'M', 'Y'}
                secondly, minutely, hourly, daily, weekly, monthly and yearly frequency.

                Multiples are also supported, i.e. '12H' for a half daily frequency.

                https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases

            full: Boolean to discard empty ending date range if True,
                Otherwise keep as is.
        """

        dates = pd.date_range(self.dates[0], self.dates[-1], freq=freq)
        old_dates = self.dates
        assert len(dates) > 1, "Selected frequency will collapse to one interval"

        assert (dates[1] - dates[0]) > (old_dates[1] - old_dates[0]), \
            "Must aggregate to a lower frequency"

        assert len(self.counters) <= len(old_dates), "Dates mismatch"

        counters = {i: TextCounter({}) for i in dates[:-1]}

        c_i = np.zeros(len(dates))

        for i, counter_i in enumerate(self.counters):

            # clip data outside the new date range
            # x > o and
            if old_dates[i] < dates[-1] and old_dates[i] >= dates[0]:
                counters[time_tag(dates, old_dates[i])] += counter_i
                c_i[date_index(old_dates[i], dates)] += 1

        # convert counters from dict to list
        counters = [i for i in counters.values()]

        # cut off last time interval if not full
        if full:
            if c_i[-1] < c_i[0]:
                dates = dates[:-1]
                counters = counters[:-1]

        if inplace:
            self.dates = dates
            self.counters = counters
            self.freq = self.dates.freq

            return
        else:
            return dates

    def plot_sentiment_timeseries_combined(self,
                                           ylims=(5, 7),
                                           unsafe=False,
                                           count_log=True,
                                           lang='en',
                                           save_pth=None,
                                           label='',
                                           color=None,
                                           ):
        """ Plot combined count and count_no_rt sentiment timeseries
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET

        """
        fig = plt.subplots(nrows=3,
                           ncols=1,
                           sharex=True,
                           figsize=(5.5, 7), dpi=500,
                           gridspec_kw={'height_ratios': [1, 3, 2]},
                           layout='constrained',
                           )

        axsub, ax, ax2 = self.plot_sentiment_timeseries(unsafe=True, fig=fig, lang=lang, count_type='count')

        agg_level = self.dates.freqstr
        counter_len = len(self.counters)
        date_length = date_range(self.dates)

        axsub, ax, ax2 = plot_formating((axsub, ax, ax2), counter_len, agg_level, date_length)

        handles, labels = ax.get_legend_handles_labels()
        ax2.legend(handles, labels, fancybox=False, frameon=False, ncol=3, loc='lower center',
                   bbox_to_anchor=(0.5, -0.65))

        axsub.set_title(self.title, fontsize=20)
        ax.set_axisbelow(True)

        ax.set_ylim(*ylims)

        print(f"Min happs: {self.happs_values.min():.2f}")
        print(f"Max happs: {self.happs_values.max():.2f}")

        if self.happs_values.min() < 5.2:
            print('resetting y axes')
            ax.set_ylim(self.happs_values.min() - 0.5, self.happs_values.max() + 0.2)
        fig[0].align_ylabels()
        # plt.subplots_adjust(left=0.2, bottom=0.3,hspace=0.00, wspace=0.00)
        if save_pth:
            figname = f"{save_pth}/{self.anchor}_sentiment_{self.dates[0].strftime('%Y-%m-%d')}_{self.dates[-1].strftime('%Y-%m-%d')}"
            plt.savefig(figname + ".pdf")
            plt.savefig(figname + ".png")

        return axsub, ax, ax2


    def plot_sentiment_timeseries_other_combined(self,
                                           other,
                                           ylims=(5, 7),
                                           unsafe=False,
                                           count_log=True,
                                           lang='en',
                                           save_pth=None,
                                           label='',
                                           color=None,
                                           ):
        """ Plot combined count and count_no_rt sentiment timeseries
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET

        """
        fig = plt.subplots(nrows=3,
                           ncols=1,
                           sharex=True,
                           figsize=(5.5, 7), dpi=500,
                           gridspec_kw={'height_ratios': [1, 3, 2]},
                           layout='constrained',
                           )
        axsub, ax, ax2 = other.plot_sentiment_timeseries(unsafe=True, fig=fig, lang=lang, count_type='count')
        axsub, ax, ax2 = self.plot_sentiment_timeseries(unsafe=True, fig=fig, lang=lang, count_type='count')

        agg_level = self.dates.freqstr
        counter_len = len(self.counters)
        date_length = date_range(self.dates)

        axsub, ax, ax2 = plot_formating((axsub, ax, ax2), counter_len, agg_level, date_length)

        handles, labels = ax.get_legend_handles_labels()
        ax2.legend(handles, labels, fancybox=False, frameon=False, ncol=3, loc='lower center',
                   bbox_to_anchor=(0.5, -0.65))

        axsub.set_title(self.title, fontsize=20)
        ax.set_axisbelow(True)

        ax.set_ylim(*ylims)

        print(f"Min happs: {self.happs_values.min():.2f}")
        print(f"Max happs: {self.happs_values.max():.2f}")

        if self.happs_values.min() < 5.2:
            print('resetting y axes')
            ax.set_ylim(self.happs_values.min() - 0.5, self.happs_values.max() + 0.2)
        fig[0].align_ylabels()
        # plt.subplots_adjust(left=0.2, bottom=0.3,hspace=0.00, wspace=0.00)
        if save_pth:
            figname = f"{save_pth}/{self.anchor}_sentiment_{self.dates[0].strftime('%Y-%m-%d')}_{self.dates[-1].strftime('%Y-%m-%d')}"
            plt.savefig(figname + ".pdf")
            plt.savefig(figname + ".png")

        return axsub, ax, ax2

    def plot_sentiment_timeseries(self,
                                  fig=None,
                                  count_type='count',
                                  ylims=(5, 7.0),
                                  unsafe=False,
                                  count_log=True,
                                  lang='en',
                                  save_pth=None,
                                  label='',
                                  color=None, ):
        """ A method to plot the ambient sentiment timeseries

        Args:
            fig: Optional;  A matplotlib tuple output of plt.subplots (figure, axs)
            count_type: A String to specify the timeseries type.
             Choose from {'freq', 'freq_no_rt', 'count', 'count_no_rt'}
            ylims: ylims for sentiment timeseries
            unsafe: flag to allow plotting even when bins have less than 1000 words.
             Don't use unsafe plots for papers!
        """
        # ToDo: make sure timezones line up for reference twitter sentiment timeseries
        # todo: make timezone ET
        df, count_df, std_df = self.happs_series(self.anchor, count_type=count_type, lang=lang)

        if not unsafe:
            assert count_df[self.anchor].median() > 1000, "Aggregate further for meaningful sentiment measurements"

        axs = sentiment_timeseries_plot(df, count_df, std_df, self.anchor,
                                        fig=fig,
                                        ylims=ylims,
                                        count_log=count_log,
                                        lang=lang,
                                        hedonometer=False,
                                        count_type=count_type,
                                        label=label,
                                        color=color,
                                        )

        if save_pth:
            count_df['sentiment'] = df[self.anchor]
            count_df['std'] = std_df[self.anchor]
            count_df['counts'] = count_df[self.anchor]
            count_df = count_df[['sentiment', 'counts', 'std']]

            fname = f"{self.title}_sentiment_timeseries_{self.dates[0].strftime('%Y-%m-%d')}" \
                    f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                    f"_freq_{str(self.freq)}_lang_{lang}.tsv"
            if type(save_pth) == str:
                fname = save_pth + fname
            else:
                fname = f'../data/sentiment/' + fname
            count_df.to_csv(fname, sep='\t', index=True)

        return axs

class AmbientGeoTweetCounters(AmbientTweetCounters):
    def __init__(self,
                 dates,
                 anchor,
                 counters=None,
                 scheme=1,
                 resolution=0.01,
                 lang='en',
                 case_sensitive=False,
                 pth=None,
                 ):
        """ Initialize Counters object with all metadata needed to reproduce query

        Attributes:
                dates: A pandas.DatatimeIndex date index for counters
                anchor: A str anchor word used to query
                counters: A list of n-grams counter objects for each time interval
                scheme: An int specifying what n-grams to parse
                resolution: A float representing subsample p to search from `driphose` (0.01),
                    `gardenhose` (0.1), or `decahose` (1.0)
                lang: A str FastText language code for initial query
                case_sensitive: A bool to query on a case sensitive anchor
                pth: An optional path for local cache
        """
        super().__init__(dates,
                         counters=counters,
                         scheme=scheme,
                         anchor=anchor
                         )  # todo: pass variable here?
        if pth is None:
            root_dir = os.path.dirname(os.path.abspath(__file__))
            pth = os.path.join(root_dir, "../data/AmbientGeoTweets/")

        self.anchor = anchor
        self.title = anchor
        self.resolution = resolution
        self.lang = lang
        self.case_sensitive = case_sensitive

        if case_sensitive is False:
            self.anchor = self.anchor.lower()

        # default save path
        self.fname = f"{pth}{anchor}_{self.dates[0].strftime('%Y-%m-%d')}" \
                     f"_{self.dates[-1].strftime('%Y-%m-%d')}" \
                     f"_resolution-{self.resolution}_freq_{str(self.freq)}_lang_{lang}_ngrams_{self.scheme}.json"
        return

    def query(self):
        """ Query the tweet database for new ambient counters"""

        collection, client = get_connection(geotweets=True)

        self.counters = get_ambient_ngrams(f'{self.anchor}',
                                           self.dates,
                                           collection,
                                           self.scheme,
                                           self.lang,
                                           self.case_sensitive
                                           )
        client.close()


def main():
    start_time = time.time()
    start_date = datetime(2010, 12, 6)

    end_date = datetime(2021, 6, 1)
    dates = pd.date_range(start_date, end_date, freq='D')
    anchors = ['Health']
    for anchor in anchors:
        print(f'Anchor: {anchor}')
        p = 0.01
        """        reddit_counters = AmbientRedditCounters(dates, anchor)
        reddit_counters.query()

        agg_level = 'W'
        if (agg_level != 'D' or agg_level != "H") and agg_level is not None:
            reddit_counters.aggregate(agg_level)
        print(f"Executed query in {time.time() - start_time:.2f}s")

        reddit_counters.plot_sentiment_timeseries_combined(save_pth='../figures/ambient_reddit/sentiment',
                                                           ylims=(5.0, 6.5))
        plt.show()
        """
        counters = AmbientTweetCounters(dates, anchor, )
        #counters.query(p=p)

        counters.mp_query(p=p)


        agg_level = 'W'
        if (agg_level != 'D' or agg_level != "H") and agg_level is not None:
            counters.aggregate(agg_level)
        print(f"Executed query in {time.time() - start_time:.2f}s")

        counters.plot_sentiment_shift_sparklines(datetime(2015, 6, 1), top_n=20)
        plt.show()

        counters.plot_sentiment_timeseries_combined(save_pth='../figures/ambient/sentiment', ylims=(5.0, 6.5))
        plt.show()

        counters.plot_pds_timeseries_combined('danger', save_pth='../figures/ambient/sentiment')
        plt.show()

        counters.plot_pds_timeseries_combined('power', save_pth='../figures/ambient/sentiment')
        plt.show()

    print('ok')

    """
    changepoint = datetime(2015,1,1)
    change = False
    if change:
        counters.plot_sentiment_shift_changepoint(changepoint, top_n=20)
        plt.savefig(f'../figures/ambient/sentiment/{anchor}.png')
        plt.show()

        f,ax = plt.subplots(figsize=(7,10))
        counters.plot_sentiment_shift_changepoint(changepoint, top_n=20, score_dict='power', ax=ax)
        plt.show()

        f,ax = plt.subplots(figsize=(6,10))
        counters.plot_sentiment_shift_changepoint(changepoint, top_n=20, score_dict='danger', ax=ax)
        plt.show()

        counters.plot_allotax_shift_changepoint(changepoint, matlab="/Users/michael/MATLAB_R2022b.app/bin/matlab -nosplash -nodesktop")

    counters.rd_matrix()

    compare = True
    if compare:
        anchor2 = 'Dog'
        counters2 = AmbientTweetCounters(dates, anchor2)
        counters2.query(p=p)

        counters2.plot_sentiment_timeseries_combined(save_pth='../figures/ambient/sentiment', ylims=(5.5, 6.5))
        plt.show()

        counters2.plot_pds_timeseries_combined('danger', save_pth='../figures/ambient/sentiment')
        plt.show()

        counters2.plot_pds_timeseries_combined('power', save_pth='../figures/ambient/sentiment')
        plt.show()

        counters.plot_allotax_shift_collapsed_comparison(counters2, matlab="/Users/michael/MATLAB_R2022b.app/bin/matlab -nosplash -nodesktop")
    return counters"""


if __name__ == "__main__":
    counters = main()
